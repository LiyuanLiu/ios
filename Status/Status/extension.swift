//
//  extension.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/14/15.
//
//

import Foundation
import UIKit
extension String {
    var length: Int {
        return self.characters.count
    }
    subscript (i:Int) -> Character{
        return self[self.startIndex.advancedBy(i)]
    }
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    func split(delimiter:String?="", limit:Int=0)->[String]
    {
        let arr = self.componentsSeparatedByString((delimiter != nil ? delimiter! : ""))
        return Array(arr[0..<(limit > 0 ? min(limit, arr.count) : arr.count)])
    }
    
    func onlyContainNum() -> Bool{
        
        for i in self.characters
        {
            if((i=="0")||(i=="1")||(i=="2")||(i=="3")||(i=="4")||(i=="5")||(i=="6")||(i=="7")||(i=="8")||(i=="9"))
            {
                
            }
            else
            {
                return false
            }
        }
        
        return true
    }
    
    subscript (r: Range<Int>) -> String? { //Optional String as return value
        get {
            let stringCount = self.characters.count as Int
            //Check for out of boundary condition
            if (stringCount < r.endIndex) || (stringCount < r.startIndex){
                return nil
            }
            let startIndex = self.startIndex.advancedBy(r.startIndex)
            
            let endIndex = self.startIndex.advancedBy(r.endIndex - r.startIndex)
            
            return self[Range(start: startIndex, end: endIndex)]
        }
    }
    func substringFromIndex(index: Int) -> String
    {
        if (index < 0 || index > self.characters.count)
        {
          //  print("index \(index) out of bounds")
            return ""
        }
        return self.substringFromIndex(self.startIndex.advancedBy(index))
    }
    func substringToIndex(index: Int) -> String
    {
        if (index < 0 || index > self.characters.count)
        {
           // print("index \(index) out of bounds")
            return ""
        }
        return self.substringToIndex(self.startIndex.advancedBy(index))
    }
   
       
}

extension NSDate{
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
extension UIViewController{
    func alert(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message:msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
   /* func getUserInput(title:String,msg:String) -> String
    {
        var temp = "aaa"
        var alertController = UIAlertController(
            title: "Email",
            message: "Please enter your email",
            preferredStyle: UIAlertControllerStyle.Alert)
        
        var okAction = UIAlertAction(
            title: "OK", style: UIAlertActionStyle.Default) {
                (action) -> Void in
                //print("You entered \((alertController.textFields?.first as UITextField).text)")
                
                temp = ((alertController.textFields?.first)! as UITextField).text!
        }
        
        alertController.addTextFieldWithConfigurationHandler {
            (txtEmail) -> Void in
            txtEmail.placeholder = "<Your email here>"
        }
        
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        return temp
        
    }*/
   
    
}







