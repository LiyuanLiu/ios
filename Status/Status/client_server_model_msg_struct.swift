//
//  client_server_model_msg_struct.swift
//  Status
//
//  Created by Liyuan Liu on 3/6/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit



//----------------------------------------------client to server postively---------------------------------------------

//-------These are structure that client send to server(short msg,to save money)----------------

/*

default msg structure

|------------default short message--------|

|<-----header------>|<-------body-------->|

|request_type | length |   message body   |

|<--4(char)-->|10(char)|<-unknown length->|


*/




/*shorter msg can save money.If no accidences happen, client will send short msg with default short head.
Otherwise, in the future,if client needs to use special bits in the msg, just add that
*/
struct _default_short_header
{
    var request_type:[Character]
    var length:[Character] //the length of the whole msg
    
}
typealias default_short_header = _default_short_header
typealias request_online_update = _default_short_header
typealias request_logout = _default_short_header
typealias request_all_policy = _default_short_header
typealias request_all_schedule = _default_short_header
typealias request_work_profile = _default_short_header
typealias request_basic_profile = _default_short_header
typealias request_edu_profile = _default_short_header



struct _request
{
    var head:default_short_header
    var username:[Character]
    var password:[Character]
    var email:[Character]//this will be null when it is login request
    
}
typealias request_login = _request
typealias request_reg = _request

struct _request_command
{
    var head:default_short_header
    var customed_status:[Character]//if it is not customed_status, this part will be null
    
}
typealias request_change_status = _request_command


//each policy and schedule will have its own index on server client will tell the server that which policy or
//schedule has been changed
struct  _request_update
{
    var head:default_short_header
    //this part resevred for update msg
}
typealias request_update_policy = _request_update
typealias request_update_schedule = _request_update
typealias request_update_profile = _request_update
typealias request_oneline_update = _request_update

struct _request_friend
{
    var head:default_short_header
    var  friend_name:[Character]
}
typealias request_search_friend = _request_friend
typealias request_add_friend = _request_friend
typealias request_del_friend = _request_friend

struct _request_chat
{
    var head:default_short_header
    var friend_name:[Character]
    var msg:[Character]
}
typealias request_chat = _request_chat

struct _request_specific
{
    var head:default_short_header
    var  name:[Character]
}
typealias request_policy = _request_specific
typealias request_schedule = _request_specific





//-------These are structure that client send to server(long head, saved for future special usage)----------------

/*
|-----------------------------------------long message structure----------------------------------------|

|<-------------------------------------header----------------------------------->|<-------body--------->|

|request_type | length |first_reserved_flag |second_reserved_flag|third_reserved_flag|   message body   |

|<--4(char)-->|10(char)|<------4(char)----->|<------4(char)----->|<-------4(char)--->|<-unknown length->|

*/


/*shorter msg can save money.If no accidences happen, client will send short msg with default short head.
Otherwise, in the future,if client needs to use special bits in the msg, just add that
*/
struct _long_header
{
    var request_type:[Character]
    var  length:[Character]//the length of the whole msg. If length string contains charactor '-', it means this is long header.The string after '-' means the real length of this msg
    var first_reserved_flag:[Character]//reserved for future usage
    var second_reserved_flag:[Character]//reserved for future usage
    var third_reserved_flag:[Character]//reserved for future usage
    
}
typealias long_header = _long_header
typealias long_request_online_update = _long_header
typealias long_request_logout = _long_header



struct _request_start
{
    var head:long_header
    var username:[Character]
    var password:[Character]
    var email:[Character]//this will be null when it is login request
    
}
typealias long_request_login = _request_start
typealias long_request_reg = _request_start

struct _long_request_command
{
    var head:long_header
    var customed_status:[Character]//if it is not customed_status, this part will be null
    
}
typealias long_request_change_status = _long_request_command

struct  _long_request_update
{
    var head:long_header
    //this part resevred for update msg
}
typealias long_request_update_policy = _long_request_update
typealias long_request_update_schedule = _long_request_update
typealias long_request_update_profile = _long_request_update
typealias long_request_oneline_update = _long_request_update

struct _long_request_friend
{
    var head:long_header
    var friend_name:[Character]
}
typealias long_request_search_friend = _long_request_friend
typealias long_request_add_friend = _long_request_friend
typealias long_request_del_friend = _long_request_friend

struct _long_request_chat
{
    var head:default_short_header
    var friend_name:[Character]
    var msg:[Character]
}
typealias long_request_chat = _long_request_chat

//------------------server replies to client negatively-----------------------------------------------------------

//these are default short structure come from server to client--------------

/*

default  reply short msg structure

|--------default short reply message------|

|<-------header------->|<-------body----->|

| reply_type  | length |   message body   |

|<--4(char)-->|10(char)|<-unknown length->|


*/

struct _reply_short_header
{
    var reply_type:[Character]
    var length:[Character]
    
    
}
typealias reply_short_header = _reply_short_header
typealias default_server_reply_short = _reply_short_header

struct _reply_all//reply for all policy or schedule
{
    var head:reply_short_header
    var num:Int
    //char all schedule_name or all policy name
    
}
typealias reply_all_policy = _reply_all
typealias reply_all_schedule = _reply_all

struct _reply_work_profile
{
    var head:reply_short_header
    var company_name:[Character]
    var position_name:[Character]
    var starttime:[Character]//its length is reserved for later
    var endtime:[Character]
    
}
typealias reply_work_profile = _reply_work_profile

struct _reply_basic_profile
{
    var head:reply_short_header
    var age:[Character]
    var location:[Character]
    var  gender:Int
    var phone_num:[Character]
    var birthday:[Character]
    
}
typealias reply_basic_profile = _reply_basic_profile

struct _reply_edu_profile
{
    var head:reply_short_header
    var junior_school:[Character]
    var j_from:[Character]
    var j_to:[Character]
    
    var high_school:[Character]
    var h_from:[Character]
    var h_to:[Character]
    
    var college_school:[Character]
    var c_from:[Character]
    var c_to:[Character]
    
    var grad_school:[Character]
    var g_from:[Character]
    var g_to:[Character]
    
    var major_college:[Character]
    var major_grad:[Character]
    
    
}
typealias reply_edu_profile = _reply_edu_profile

//-----these are long structure come from server to client--------------

/*

reply long msg structure

|-----------------------------------------long reply structure-----------------------------------------|

|<-------------------------------------header-------------------------------------->|<-------body----->|

|reply_type  | length |first_reserved_flag |second_reserved_flag|third_reserved_flag|   message body   |

|<--4(char)->|10(char)|<------4(char)----->|<------4(char)----->|<-------4(char)--->|<-unknown length->|

*/

struct _reply_short_header_l
{
    var reply_type:[Character]
    var length:[Character]
    var first_reserved_flag:[Character]//reserved for future usage
    var second_reserved_flag:[Character]//reserved for future usage
    var third_reserved_flag:[Character]//reserved for future usage
    
    
}
typealias reply_long_header = _reply_short_header_l
typealias server_reply_long = _reply_short_header_l


//-------------server send to client postively(default short msg)--------------
struct _notice_short_header
{
    var notice_type:[Character]
    var length:[Character]
    
}
typealias short_notice_header = _notice_short_header

struct _short_add_you
{
    var head:short_notice_header
    var sender_name:[Character]
}
typealias short_add_you = _short_add_you

struct _short_chat_to_you
{
    var head:short_notice_header
    var sender_name:[Character]
    var chat_masg:[Character]
}
typealias short_chat = _short_chat_to_you

//-------------server send to client postively(long msg)--------------
struct _notice_long_header
{
    var notice_type:[Character]
    var length:[Character]
    var first_reserved_flag:[Character]//reserved for future usage
    var second_reserved_flag:[Character]//reserved for future usage
    var third_reserved_flag:[Character]//reserved for future usage
    
}
typealias long_notice_header = _notice_long_header

struct _long_add_you
{
    var head:long_notice_header
    var sender_name:[Character]
}
typealias long_add_you = _long_add_you

struct _long_chat_to_you
{
    var head:long_notice_header
    var sender_name:[Character]
    var chat_masg:[Character]
}
typealias long_chat = _long_chat_to_you

//-----------------client replies to server(negatively)-------------------------------
struct _answer_short_header
{
    var notice_type:[Character]
    var length:[Character]
    
}
typealias short_answer_header = _answer_short_header

struct _answer_long_header_
{
    var notice_type:[Character]
    var length:[Character]
    var first_reserved_flag:[Character]//reserved for future usage
    var second_reserved_flag:[Character]//reserved for future usage
    var third_reserved_flag:[Character]//reserved for future usage
    
}
typealias longer_notice_header = _answer_long_header_





































