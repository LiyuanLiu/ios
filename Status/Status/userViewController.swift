//
//  userViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/2/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit

class userViewController: UIViewController,UIPopoverPresentationControllerDelegate {
    
    let picker = UIImageView(image: UIImage(named: "picker"))
    
    func createPicker()
    {
       
        picker.frame = CGRect(x: 250, y: 100, width: 130, height: 130)
        picker.alpha = 1.0
        picker.hidden = true
        picker.userInteractionEnabled = true
       // picker.backgroundColor = UIColor.greenColor()
        self.view.addSubview(picker)
        
        
    }
    
    
    func clickProfile(sender:UIButton!)
    {
        print("profile")
        performSegueWithIdentifier("profileSegue", sender: self)
    }
    func clickPolicy(sender:UIButton!)
    {
        print("clickPolicy")
        performSegueWithIdentifier("policySegue", sender: self)
    }
    func clickSchedule(sender:UIButton!)
    {
        print("clickSchedule")
        performSegueWithIdentifier("scheduleSegue", sender: self)
    }
    func clickLogout(sender:UIButton!)
    {
        print("clickLogout")
        performSegueWithIdentifier("profileSegue", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.bringSubviewToFront(welcomeUser)
        
        if username != nil
        {
            welcomeUser.text = "Hello,\(username!)"
        }
        self.picker.userInteractionEnabled = true
        createPicker()
        
        bringObjectToFront()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func myAccount(sender: UIButton) {
        
        picker.hidden ? openPicker() : closePicker()
    }
    
    
    func openPicker()
    {
        self.picker.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                
                self.picker.frame = CGRect(x: 250, y: 100, width: 130, height: 130)
                self.picker.alpha = 1
            })
        
        
        var offset = 0
        
        
        let buttonProfile:UIButton = UIButton(type: UIButtonType.Custom) as UIButton
        buttonProfile.frame = CGRect(x: 1, y: offset, width: 120, height: 43)
        
        
        buttonProfile.setTitleColor(UIColor(red: 0.85, green: 0.22, blue: 0.71, alpha: 1.0), forState: .Normal)
        //  button.setTitleColor(UIColor(rgba: feeling["color"]!), forState: .Normal)
        buttonProfile.setTitle("My profile", forState: .Normal)
        buttonProfile.tag = 0
        buttonProfile.addTarget(self, action: "clickProfile:", forControlEvents: UIControlEvents.TouchUpInside)
        
        picker.addSubview(buttonProfile)
        
        offset += 30
        
        let buttonPolicy = UIButton()
        buttonPolicy.frame = CGRect(x: 1, y: offset, width: 120, height: 43)
        buttonPolicy.setTitleColor(UIColor(red: 0.25, green: 0.22, blue: 0.71, alpha: 1.0), forState: .Normal)
        //  button.setTitleColor(UIColor(rgba: feeling["color"]!), forState: .Normal)
        buttonPolicy.setTitle("My policy", forState: .Normal)
        buttonPolicy.tag = 1
        buttonPolicy.addTarget(self, action: "clickPolicy:", forControlEvents: UIControlEvents.TouchUpInside)
        
        picker.addSubview(buttonPolicy)
        
        offset += 30
        
        let buttonSchedule = UIButton()
        buttonSchedule.frame = CGRect(x: 1, y: offset, width: 120, height: 43)
        buttonSchedule.setTitleColor(UIColor(red: 0.25, green: 0.72, blue: 0.71, alpha: 1.0), forState: .Normal)
        //  button.setTitleColor(UIColor(rgba: feeling["color"]!), forState: .Normal)
        buttonSchedule.setTitle("My schedule", forState: .Normal)
        buttonSchedule.tag = 2
        buttonSchedule.addTarget(self, action: "clickSchedule:", forControlEvents: UIControlEvents.TouchUpInside)
        
        picker.addSubview(buttonSchedule)
        
        offset += 30
        
        let buttonLogout = UIButton()
        buttonLogout.frame = CGRect(x: 1, y: offset, width: 120, height: 43)
        buttonLogout.setTitleColor(UIColor(red: 0.25, green: 0.52, blue: 0.21, alpha: 1.0), forState: .Normal)
        //  button.setTitleColor(UIColor(rgba: feeling["color"]!), forState: .Normal)
        buttonLogout.setTitle("Logout", forState: .Normal)
        buttonLogout.tag = 3
        buttonLogout.addTarget(self, action: "clickLogout:", forControlEvents: UIControlEvents.TouchUpInside)
        
        picker.addSubview(buttonLogout)
        
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                
                self.picker.frame = CGRect(x: 250, y: 100, width: 130, height: 130)
                self.picker.alpha = 0
                           },
            completion: { finished in
                self.picker.hidden = true
            }
        )
    }
    
    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(myAccountButton)
        self.view.bringSubviewToFront(welcomeUser)
        self.view.bringSubviewToFront(picker)
    }
    
    @IBOutlet weak var myAccountButton: UIButton!
    
    
    var username:String?
    
    @IBOutlet weak var welcomeUser: UILabel!
}
