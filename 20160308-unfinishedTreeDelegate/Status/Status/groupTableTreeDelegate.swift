//
//  groupTableTreeModel.swift
//  Status
//
//  Created by Liyuan Liu on 3/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit
protocol groupTableTreeDelegate{
    
   
    
    func numOfSectionsInGroupTableTree() -> Int
   // func groupTableTreeView(groupTableView: groupTableTreeView,numofRowsInSection section:Int) -> Int
    
    func groupTableTreeView(groupTableView: UITableView,numofRowsInSection section:Int) -> Int
    
    //this cell contains root and children
    //func groupTableTreeViewCell(groupTableView:groupTableTreeView,cellForRowAtIndexPath indexPath:NSIndexPath,identifier:String) ->groupTableTreeCell
    func groupTableTreeViewCell(groupTableView:UITableView,cellForRowAtIndexPath indexPath:NSIndexPath) ->groupTableTreeCell
    
    //optional func groupTableTreeView(groupTableView:groupTableTreeView,didSelectRootRowAtIndexPath indexPath:NSIndexPath)
    func groupTableTreeView(groupTableView:UITableView,didSelectRootRowAtIndexPath indexPath:NSIndexPath)
    
    func returnIdentfier() -> String
    
}

