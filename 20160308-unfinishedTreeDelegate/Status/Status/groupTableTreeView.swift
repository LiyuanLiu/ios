//
//  groupTableTreeView.swift
//  Status
//
//  Created by Liyuan Liu on 3/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit
class groupTableTreeView:UITableView{
    
    var groupDelegate:groupTableTreeDelegate?
    
    var numOfGroupSections:Int
    var cellSet:[groupTableTreeCell]?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        self.numOfGroupSections = (groupDelegate?.numOfSectionsInGroupTableTree())!
        
        super.init(frame: frame, style: style)
        
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}

extension groupTableTreeView:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        
        return self.numOfGroupSections
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        return (groupDelegate?.groupTableTreeView(tableView, numofRowsInSection: section))!
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let groupCell = groupDelegate?.groupTableTreeViewCell(tableView, cellForRowAtIndexPath: indexPath)
        let cell:UITableViewCell
        
        cell = tableView.dequeueReusableCellWithIdentifier((groupDelegate?.returnIdentfier())!, forIndexPath: indexPath)
        
        cell.textLabel?.text = groupCell?.rootLabel
        
        return cell
        
    }
    

}