//
//  addScheduleViewController.swift
//  Status
//
//  Created by Liyuan Liu on 3/5/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit

class addScheduleViewController: UIViewController,UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        timePicker.hidden = true
        
        startTimeText.delegate = self
        endTimeText.delegate = self
        periodNameText.delegate = self
        descriptionText.delegate = self
        newScheduleNameText.delegate = self
        
        bringObjectToFront()
        
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        timePicker.hidden = false
        whichTimeText = textField
        
        return false
    }
    
    
    @IBAction func saveSchedule(sender: AnyObject) {
        
        
        //save data
        
        //back to previouse view controller
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }

    @IBAction func addPeriod(sender: AnyObject) {
        
        if((endtime != nil) && (starttime != nil ))
        {
            if((endtime?.isLessThanDate(starttime!)) == true)
            {
                alert("Error", msg:  "End time cannot be less than start time!")
                //PopUIAlert("Error", message: "End time cannot be less than start time!")
            }
            else
            {
                alert("Add Period", msg: "You have added a new period")
                //PopUIAlert("Add Period", message: "You have added a new period")
            }
        }
        else if((endtime == nil)||(starttime == nil))
        {
            alert("Error", msg: "End time cannot be blanket!")
            //PopUIAlert("Error", message: "End time cannot be blanket!")
        }
        
        //save data
        
        newScheduleNameText.text = ""
        periodNameText.text = ""
        startTimeText.text = ""
        endTimeText.text = ""
        descriptionText.text = ""
    }
   
    
    @IBAction func selectTime(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.timeStyle = .ShortStyle
        
        let date = dateFormatter.stringFromDate(timePicker.date)
        
        if(whichTimeText == endTimeText)
        {
            endTimeText.text = date
            
            endtime = timePicker.date
            
            endTimeText.reloadInputViews()
        }
        else
        {
            startTimeText.text = date
           
            starttime = timePicker.date
            
            startTimeText.reloadInputViews()
        }
        timePicker.hidden = true
        
    }
    
    
    var whichTimeText:UITextField?
    var endtime:NSDate?
    var starttime:NSDate?
    
    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(timePicker)
        
        self.view.bringSubviewToFront(newScheduleNameLabel)
        self.view.bringSubviewToFront(newScheduleNameText)
        
        self.view.bringSubviewToFront(addPeriodButton)
        
        self.view.bringSubviewToFront(periodNameLabel)
        self.view.bringSubviewToFront(periodNameText)
        
        self.view.bringSubviewToFront(whichPolicyLabel)
        self.view.bringSubviewToFront(whichPolicyPicker)
        
        self.view.bringSubviewToFront(startTimeLabel)
        self.view.bringSubviewToFront(startTimeText)
        
        self.view.bringSubviewToFront(endTimeLabel)
        self.view.bringSubviewToFront(endTimeText)
        
        self.view.bringSubviewToFront(descriptionLabel)
        self.view.bringSubviewToFront(descriptionText)
    
    }
    
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet weak var newScheduleNameLabel: UILabel!
    @IBOutlet weak var newScheduleNameText: UITextField!
    
    @IBOutlet weak var addPeriodButton: UIButton!
    
    @IBOutlet weak var periodNameLabel: UILabel!
    @IBOutlet weak var periodNameText: UITextField!
    
    @IBOutlet weak var whichPolicyLabel: UILabel!
    @IBOutlet weak var whichPolicyPicker: UIPickerView!
    
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var startTimeText: UITextField!
    
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var endTimeText: UITextField!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextField!
    
}





















