//
//  groupTableTreeDataSource.swift
//  Status
//
//  Created by Liyuan Liu on 3/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

class groupTableTreeCell {
    var rootLabel:String
    var rootImage: UIImage?
    var children:[groupTableTreeCell]?
    
    init(rootLabel:String,rootImage:UIImage? = nil, children:[groupTableTreeCell]? = [])
    {
        self.rootLabel = rootLabel
        self.rootImage = rootImage
        self.children = children
    }
}
/*protocol groupTableTreeDataSource{
    func rootCellWithChildren() -> groupTableTreeCell
    
}*/

