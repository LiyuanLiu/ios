//: Playground - noun: a place where people can play

import UIKit
import XCPlayground
@testable import Status

let containerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 375.0, height: 667.0))
XCPShowView("Container View", view: containerView)


let picker = UIImageView(image: UIImage(named: "picker"))
picker.frame = CGRect(x: ((containerView.frame.width / 2) - 143), y: 200, width: 286, height: 291)

picker.backgroundColor = UIColor.redColor()

containerView.addSubview(picker)


let buttonSchedule = UIButton()
buttonSchedule.frame = CGRect(x: 13, y: 21, width: 260, height: 43)
//buttonSchedule.setTitleColor(UIColor(red: 0.25, green: 0.72, blue: 0.71, alpha: 1.0), forState: .Normal)

buttonSchedule.backgroundColor = UIColor.greenColor()

containerView.addSubview(buttonSchedule)

let circle = UIImageView(frame: CGRect(x: 220, y: -180.0, width: 50.0, height: 50.0))
//circle.center = containerView.center
//circle.layer.cornerRadius = 25.0

let startingColor = UIColor(red: (253.0/255.0), green: (159.0/255.0), blue: (47.0/255.0), alpha: 1.0)
circle.backgroundColor = startingColor

containerView.addSubview(circle);

let rectangle = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 50.0, height: 50.0))
rectangle.center = containerView.center
rectangle.layer.cornerRadius = 5.0

rectangle.backgroundColor = UIColor.whiteColor()

containerView.addSubview(rectangle)

UIView.animateWithDuration(2.0, animations: { () -> Void in
    let endingColor = UIColor(red: (255.0/255.0), green: (61.0/255.0), blue: (24.0/255.0), alpha: 1.0)
    circle.backgroundColor = endingColor
    
    let scaleTransform = CGAffineTransformMakeScale(5.0, 5.0)
    
    circle.transform = scaleTransform
    
    let rotationTransform = CGAffineTransformMakeRotation(3.14)
    
    rectangle.transform = rotationTransform
})

