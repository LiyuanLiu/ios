//
//  groupTreeView.swift
//  Status
//
//  Created by Liyuan Liu on 3/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

struct groupCell{
    var sectionHeader:String
    
    var childrenExist: Bool
}