//
//  ViewController.swift
//  Status
//
//  Created by Liyuan Liu on 1/27/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit

class loginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        accountWarningLabel.text = ""
        passwordWarningLabel.text = ""
        bringObjectToFront()
        
        
        
        
    }

    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(statusLabel)
        self.view.bringSubviewToFront(accountLabel)
        self.view.bringSubviewToFront(accountText)
        self.view.bringSubviewToFront(passwordLabel)
        self.view.bringSubviewToFront(passwordText)
        self.view.bringSubviewToFront(accountWarningLabel)
        self.view.bringSubviewToFront(passwordWarningLabel)
        self.view.bringSubviewToFront(loginButton)
        self.view.bringSubviewToFront(wordsLabel)
        self.view.bringSubviewToFront(registerButton)
        self.view.bringSubviewToFront(introButton)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier
        {
        case .Some("loginSegue"):
            
            let tabbar = segue.destinationViewController as! UITabBarController
            
            let navController = tabbar.viewControllers![0] as! UINavigationController
            
            let destViewController = navController.viewControllers[0] as! userViewController
            
            destViewController.username = accountText!.text
            
        case .Some("registerSegue"):
            let register = segue.destinationViewController as! registerViewController
            register.username = accountText!.text
            
            register.password = passwordText!.text
            
            
        default:
            
            break
        }
    }
   
    @IBOutlet weak var statusLabel: UILabel!

    @IBOutlet weak var accountLabel: UILabel!
    
    @IBOutlet weak var accountText: UITextField!
    
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var accountWarningLabel: UILabel!
    
    @IBOutlet weak var passwordWarningLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var wordsLabel: UILabel!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var introButton: UIButton!
    
}

