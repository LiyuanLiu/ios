//
//  eduViewConrtoller.swift
//  Status
//
//  Created by Liyuan Liu on 2/9/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit
class eduProfileViewController:UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
       
        bringObjectToFront()
        
        
    }
    
    @IBAction func saveEduInfo(sender: AnyObject) {
        
        //send to server
    }
    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(juniorSchoolLabel)
        self.view.bringSubviewToFront(juniorSchoolText)
        
        self.view.bringSubviewToFront(juniorFromLabel)
        self.view.bringSubviewToFront(juniorFromText)
        
        self.view.bringSubviewToFront(juniorToLabel)
        self.view.bringSubviewToFront(juniorToText)
        
        self.view.bringSubviewToFront(highSchoolLabel)
        self.view.bringSubviewToFront(highSchoolText)
        
        self.view.bringSubviewToFront(highFromLabel)
        self.view.bringSubviewToFront(highFromText)
        
        self.view.bringSubviewToFront(highToLabel)
        self.view.bringSubviewToFront(highToText)
        
        self.view.bringSubviewToFront(collegeLabel)
        self.view.bringSubviewToFront(collegeText)
        
        self.view.bringSubviewToFront(collegeFromLabel)
        self.view.bringSubviewToFront(collegeFromText)
        
        self.view.bringSubviewToFront(collegeToLabel)
        self.view.bringSubviewToFront(collegeToText)
        
        self.view.bringSubviewToFront(gradSchoolLabel)
        self.view.bringSubviewToFront(gradSchoolText)
        
        self.view.bringSubviewToFront(gradFromLabel)
        self.view.bringSubviewToFront(gradFromText)
        
        self.view.bringSubviewToFront(gradToLabel)
        self.view.bringSubviewToFront(gradToText)
        
        self.view.bringSubviewToFront(majorLabel)
        self.view.bringSubviewToFront(majorCollegeText)
        self.view.bringSubviewToFront(majorGradText)
    }
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var juniorSchoolLabel: UILabel!
    @IBOutlet weak var juniorSchoolText: UITextField!
    
    @IBOutlet weak var juniorFromLabel: UILabel!
    @IBOutlet weak var juniorFromText: UITextField!
   
    @IBOutlet weak var juniorToLabel: UILabel!
    @IBOutlet weak var juniorToText: UITextField!
    
    @IBOutlet weak var highSchoolLabel: UILabel!
    @IBOutlet weak var highSchoolText: UITextField!
    
    @IBOutlet weak var highFromLabel: UILabel!
    @IBOutlet weak var highFromText: UITextField!
    
    @IBOutlet weak var highToLabel: UILabel!
    @IBOutlet weak var highToText: UITextField!
    
    @IBOutlet weak var collegeLabel: UILabel!
    @IBOutlet weak var collegeText: UITextField!
    
    @IBOutlet weak var collegeFromLabel: UILabel!
    @IBOutlet weak var collegeFromText: UITextField!
    
    @IBOutlet weak var collegeToLabel: UILabel!
    @IBOutlet weak var collegeToText: UITextField!
    
    @IBOutlet weak var gradSchoolLabel: UILabel!
    @IBOutlet weak var gradSchoolText: UITextField!
    
    
    @IBOutlet weak var gradFromLabel: UILabel!
    @IBOutlet weak var gradFromText: UITextField!
    
    @IBOutlet weak var gradToLabel: UILabel!
    @IBOutlet weak var gradToText: UITextField!
    
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var majorCollegeText: UITextField!
    @IBOutlet weak var majorGradText: UITextField!
    
    
}



















