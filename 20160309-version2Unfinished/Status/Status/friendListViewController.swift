//
//  friendListViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/11/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

class friendListViewController:UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    var groupSection = [String: [String]]()
    
    var isClick = [Bool]()
    
    
   
    @IBOutlet weak var friend: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       groupSection["gtest1"] = ["subt1","subt2"]
       groupSection["gtest2"] = ["2sub1","2sub2"]
    
        for _ in 0..<groupSection.count
        {
            isClick.append(false)
            
        }
        friend.delegate = self
        
        // Do any additional setup after loading the view.
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return groupSection.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let index = groupSection.startIndex.advancedBy(section)
        //if groupSection[groupSection.keys[index]] != NilLiteralConvertible{
            return (groupSection[groupSection.keys[index]]?.count)!
        //}
        
        //return 2
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,50))
        
        
        let headerLabel = UILabel(frame: CGRectMake(10,0,self.view.frame.size.width-20-50,50))
        let index = groupSection.startIndex.advancedBy(section)
        headerView.tag = section
        headerLabel.text = groupSection.keys[index]
        headerView.addSubview(headerLabel)
        
        return headerView
        
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRectZero)
        return footer
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if isClick[indexPath.section]
        {
            return 50
        }
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("groupFriendCell")
        if cell == nil
        {
            cell?.textLabel?.text = "nil"
            cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            
        }
        if isClick[indexPath.section]
        {
            let index = groupSection.startIndex.advancedBy(indexPath.section)
            cell?.textLabel!.text = groupSection[groupSection.keys[index]]![indexPath.row]
            
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("here")
       
        let temp = isClick[indexPath.section]
        
        isClick[indexPath.section] = !temp
        
        let range = NSMakeRange(indexPath.section, 1)
        let sectionToReload = NSIndexSet(indexesInRange: range)
        
        tableView.reloadSections(sectionToReload, withRowAnimation: UITableViewRowAnimation.Fade)
        
    }
    
  /*  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("groupFriendCell", forIndexPath: indexPath)
        let viewModel = displayedRows[indexPath.row]
        cell.textLabel!.text = viewModel.rootLabel
        //cell.textLabel?.text = "test"
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let viewModel = displayedRows[indexPath.row]
        if viewModel.children.count > 0 {
            let range = indexPath.row+1...indexPath.row+viewModel.children.count
            let indexPaths = range.map{return NSIndexPath(forRow: $0, inSection: indexPath.section)}
            tableView.beginUpdates()
            if viewModel.isCollapsed {
                displayedRows.insertContentsOf(viewModel.children, at: indexPath.row+1)
                
                print("\(displayedRows.count)")
                print("\(displayedRows[indexPath.row+1].rootLabel)")
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            } else {
                displayedRows.removeRange(range)
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            }
            tableView.endUpdates()
        }
        viewModel.isCollapsed = !viewModel.isCollapsed
    }*/
    
    
    
    
}









