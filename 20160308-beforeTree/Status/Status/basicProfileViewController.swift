//
//  basicProfile.swift
//  Status
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class basicProfileViewController:UIViewController,CLLocationManagerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
//receiving msg from server and then show them
        ageLabel.text = ""//=receving msg from server
        locationLabel.text = ""
        //gender
        //phoneNum
        
        phoneNumText.delegate = self
        
        genderPicker.dataSource = self
        genderPicker.delegate = self
        
        // Do any additional setup after loading the view.
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        self.birthdayPicker.maximumDate = NSDate()
        
        
    }

    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderOption.count
    }
  /*  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderOption[row]
    }*/
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("select \(genderOption[row])")
        
        //send to server when I click save button
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = genderOption[row]
        let mytitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.blackColor()])
        pickerLabel.attributedText = mytitle
        let hue = CGFloat(row)/(CGFloat(genderOption.count))
        pickerLabel.backgroundColor = UIColor(hue:hue,saturation: 0.5, brightness:1.0, alpha: 1.0)
        pickerLabel.textAlignment = .Center
        return pickerLabel
        
    }
    
    @IBAction func saveBasicInfo(sender: AnyObject) {
        
        //get info and send to server
        
        if(phoneNumText.text != nil)
        {
            let temp_string = phoneNumText.text!
            
            
            
            if(temp_string.length >= LENGTH_MACRO.PHONE_NUM_LENGTH.REQUEST_TYPE_LENGTH_MAX)
            {
                alert("Length", msg: "phone num cannot longer than \(LENGTH_MACRO.PHONE_NUM_LENGTH.REQUEST_TYPE_LENGTH_MAX)")
                
                
                
            }
            
            if(temp_string.onlyContainNum())
            {
                
            }
            else
            {
                alert("Num", msg: "Phone num can only contain nums")
                
            }

            
        }
        else
        {
            //using inital value to fill the string and send to server
        }
        
        
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let geoCoder = CLGeocoder()
        let current_a_h = self.locationManager.location
        let currentLocation = CLLocation(latitude: current_a_h!.coordinate.latitude, longitude: current_a_h!.coordinate.longitude)
        
        
        
        geoCoder.reverseGeocodeLocation(currentLocation, completionHandler: {
            placemarks, error in
            
            if error == nil && placemarks!.count > 0 {
                self.placeMark = placemarks!.last as CLPlacemark!
               // print("\(self.placeMark!.thoroughfare)\n\(self.placeMark!.postalCode) \(self.placeMark!.locality)\n\(self.placeMark!.country))")
                self.locationLabel.text = "\(self.placeMark!.locality!)"
               self.locationLabel.reloadInputViews()
                self.locationManager.stopUpdatingLocation()
            }
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location \(error.localizedDescription)")
        
    }
        
    @IBAction func birthdayPickerAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let date = dateFormatter.stringFromDate(birthdayPicker.date)
        
        let current_date = NSCalendar.currentCalendar()
        let year = current_date.component(.Year, fromDate: NSDate())
        
        
        let age = Int(year) - Int(date.substringFromIndex(6))!
        
        ageLabel.text = "\(age)"
        ageLabel.reloadInputViews()
        
        //then pass this date and age to server
       
        
        
    }
    var placeMark: CLPlacemark!
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
   let genderOption = ["Male","Female","Transgender","Gender Fluid","Two-Spirit","Transmasculine","Transsexual","Cisgender","Unknown"]
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var genderPicker: UIPickerView!
    
    @IBOutlet weak var phoneNumLabel: UILabel!
    @IBOutlet weak var phoneNumText: UITextField!
    
    @IBOutlet weak var ageLabelLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var birthdayPicker: UIDatePicker!
    
    
    
}






















