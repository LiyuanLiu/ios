//
//  common_macro.swift
//  Status
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation

public enum LENGTH_MACRO:Int
{
    case REQUEST_TYPE_LENGTH_MAX
    case REPLY_TYPE_LENGTH_MAX
    case NOTICE_TYPE_LENGTH_MAX
    case ANSWER_TYPE_LENGTH_MAX
    case AGE_LENGTH
    case LOCATION_LENGTH
    case BIRTHDAY_LENGTH
    case LENGTH_STRING_MAX //When send the string of length, the max size of the string of length
    case PHONE_NUM_LENGTH
    case PASSWORD_MAX
    case USERNAME_MAX
    case SCHEDULE_NAME_MAX
    case COMPANY_NAME_MAX
    case POSITION_NAME_MAX
    case SCHOOLE_NAME_MAX
    case MAJOR_NAME_MAX
    case POLICY_NAME_MAX
    case EMAIL_ADDR_MAX
    case CUSTOMIZED_STATUS_MAX
    case CHATTING_MSG_MAX
    
    
    var REQUEST_TYPE_LENGTH_MAX:Int
    {
        get {
            switch(self){
            case REQUEST_TYPE_LENGTH_MAX,REPLY_TYPE_LENGTH_MAX,NOTICE_TYPE_LENGTH_MAX,ANSWER_TYPE_LENGTH_MAX,AGE_LENGTH:
                return 4
            case LENGTH_STRING_MAX:
                return 10
            case BIRTHDAY_LENGTH:
                return 12
            case LOCATION_LENGTH:
                return 50
            case PASSWORD_MAX,PHONE_NUM_LENGTH:
                return 20
            case USERNAME_MAX:
                return 50
            case SCHEDULE_NAME_MAX:
                return 50
            //belong to work profile
            case COMPANY_NAME_MAX:
                return 50
            case POSITION_NAME_MAX:
                return 50
                
            //belong to edu profile
            case SCHOOLE_NAME_MAX:
                return 50
            case MAJOR_NAME_MAX:
                return 50
                
            case POLICY_NAME_MAX:
                return 50
            case EMAIL_ADDR_MAX:
                return 100
            case CUSTOMIZED_STATUS_MAX:
                return 150
            case CHATTING_MSG_MAX:
                return 1024
            
                
            }
        }
    
    }
    
}
public enum client_Init_Value:String
{
    case INIT_VALUE = "#"
    case SEPERATOR = "|"
    var description:String{
        get{
            return self.rawValue
        }
    }
}
public enum STATUS_USER:String
{
    case AVALIABLE =         "300"
    case NOT_AVALIABLE =     "301"
    case BUSY =              "302"
    case CUSTOMIZED_STATUS = "400"
    var description:String{
        get{
            return self.rawValue
        }
    }
}







