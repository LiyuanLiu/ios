//
//  GlanceController.swift
//  iwatch Extension
//
//  Created by Liyuan Liu on 1/14/16.
//
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {
    
    override init() {
        super.init()
        
        
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
