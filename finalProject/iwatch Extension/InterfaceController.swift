//
//  InterfaceController.swift
//  iwatch Extension
//
//  Created by Liyuan Liu on 1/14/16.
//
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate {
    @IBOutlet var label: WKInterfaceLabel!
    var session:WCSession!
    
    override init() {
        super.init()
        
        label.setText("Project")
        
      
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
      
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if(WCSession.isSupported())
        {
            self.session = WCSession.defaultSession()
            self.session.delegate = self
            self.session.activateSession()
        }
        
       
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
       
    }
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        self.label.setText(message["a"]! as? String)
    }

}












