//
//  FinalProjectUITests.swift
//  FinalProjectUITests
//
//  Created by Charles Augustine on 11/4/15.
//
///Users/liyuanliu/Documents/lecture/ios/Repos/final_project/first/finalProject/FinalProjectUITests

import XCTest

class FinalProjectUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        
        
        
        let app = XCUIApplication()
        let textField = app.otherElements.containingType(.NavigationBar, identifier:"Login").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.TextField).element
        textField.tap()
        textField.typeText("abc")
        
        let registerButton = app.buttons["Register"]
        registerButton.tap()
        
        XCTAssert(app.buttons["Reset"].exists)
        XCTAssert(app.buttons["Register"].exists)
        
        XCTAssert(app.navigationBars["Register"].exists)
        
        
        
        let element = app.otherElements.containingType(.NavigationBar, identifier:"Register").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
        let secureTextField = element.childrenMatchingType(.SecureTextField).elementBoundByIndex(0)
        secureTextField.tap()
        
        secureTextField.typeText("1234")
        
        let secureTextField2 = element.childrenMatchingType(.SecureTextField).elementBoundByIndex(1)
        secureTextField2.tap()
        secureTextField2.tap()
        secureTextField2.typeText("1234")
        registerButton.tap()
        
        XCTAssert(app.buttons["Online"].exists)
        XCTAssert(app.buttons["Busy"].exists)
        XCTAssert(app.buttons["Invisible"].exists)
        XCTAssert(app.buttons["Take Photo"].exists)
        
        XCTAssert(app.buttons["Pick from Library"].exists)
        
        XCTAssert(app.buttons["Check"].exists)
        XCTAssert(app.buttons["Logout"].exists)
        XCTAssert(app.buttons["Black"].exists)
        
        XCTAssert(app.navigationBars["Functions"].exists)
        
        
        app.buttons["Check"].tap()
        XCTAssert(app.navigationBars["All Users Online"].exists)
        app.navigationBars["All Users Online"].buttons["Functions"].tap()
        
        
        app.buttons["Black"].tap()
        XCTAssert(app.navigationBars["Black List"].exists)
        app.navigationBars["Black List"].buttons["Functions"].tap()
        
        
        app.buttons["Busy"].tap()
        
        XCTAssert(app.staticTexts["Busy"].exists)
        
        app.buttons["Invisible"].tap()
        XCTAssert(app.staticTexts["Invisible"].exists)
        app.buttons["Online"].tap()
        XCTAssert(app.staticTexts["Online"].exists)
        
        
        
        
        
        
              
        
        
        
        
        
        
        
        
    }
  

    
    
}
