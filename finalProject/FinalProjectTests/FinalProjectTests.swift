//
//  FinalProjectTests.swift
//  FinalProjectTests
//
//  Created by Charles Augustine on 7/14/15.
//
//

import UIKit
import XCTest
@testable import FinalProject
class FinalProjectTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFriend() {
        let resultController = catFriend.sharedCatFriendsService.catFriends()
        try! resultController.performFetch()
        let num = resultController.sections!.count
        XCTAssertGreaterThan(resultController.sections!.count, 0, "should be greater than 0")
        
        for var i = 0; i < num;i++
        {
            //var items = resultsController.sections![i].numberOfObjects
            XCTAssertGreaterThan(resultController.sections![i].numberOfObjects, 0, "should be greater than 0")
        }
        
        
        let black = catFriend.sharedCatFriendsService.catBlack()
        try! black.performFetch()
        let black_num = black.sections!.count
        XCTAssertGreaterThan(black.sections!.count, 0, "should be greater than 0")
        
        for var i = 0; i < black_num;i++
        {
            //var items = resultsController.sections![i].numberOfObjects
            XCTAssertGreaterThan(black.sections![i].numberOfObjects, 0, "should be greater than 0")
        }
        
    }
    
    
}
