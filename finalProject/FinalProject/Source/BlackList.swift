//
//  BlackList.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/16/15.
//
//

import Foundation
import UIKit
import CoreData
class BlackList:UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
      if resultsController!.sections![section].numberOfObjects > 1
      {
       // print("section---------- is \(section)")
          return resultsController!.sections![section].numberOfObjects - 1
      }
       else
      {
       // print("section---------- is \(section)")
        flag = 1
        return 1
        
       }
        
        
       // return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell
        let black = resultsController!.objectAtIndexPath(indexPath) as! Black
        cell = tableView.dequeueReusableCellWithIdentifier("black", forIndexPath: indexPath)
        
        if flag == 1
        {
            cell.textLabel!.text = "You don't have any names in your blacklist"
            
        }
        else
        {
            cell.textLabel!.text = black.name
        }
        
        return cell
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let resultController = /*try?*/ catFriend.sharedCatFriendsService.catBlack(nil)
        try! resultController.performFetch()
        self.resultsController = resultController
        
        
    }
    var flag:Int?
  
    private var resultsController:NSFetchedResultsController?
    

}