//
//  Black+CoreDataProperties.swift
//  FinalProject
//
//  Created by Liyuan Liu on 12/11/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Black {

    @NSManaged var name: String?

}
