//
//  catFriend.swift
//  FinalProject
//
//  Created by Liyuan Liu on 12/5/15.
//
//

import Foundation
import CoreData
import CoreDataService
class catFriend{
    func catFriends() -> NSFetchedResultsController{
        let fetchRequest = NSFetchRequest(namedEntity:Friends.self)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        let context = CoreDataService.sharedCoreDataService.mainQueueContext
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
    }
    func catBlack(black: String? = nil) -> NSFetchedResultsController{
        let fetchRequest = NSFetchRequest(namedEntity:Black.self)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        if black != nil
        {
            fetchRequest.predicate = NSPredicate(format: "name == %@", black!)
        }
        else
        {
            
        }
        
        
        let context = CoreDataService.sharedCoreDataService.mainQueueContext
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
    }
    private init(){
        let context = CoreDataService.sharedCoreDataService.mainQueueContext
        context.performBlockAndWait{
            let fetchRequest = NSFetchRequest(namedEntity: Friends.self)
            let blackfetchRequest = NSFetchRequest(namedEntity: Black.self)
            if context.countForFetchRequest(fetchRequest, error: nil)==0{
                let friend = NSEntityDescription.insertNewObjectForNamedEntity(Friends.self, inManagedObjectContext: context)
                friend.name = "test"
            }
            if context.countForFetchRequest(blackfetchRequest, error: nil)==0
            {
                let blackname = NSEntityDescription.insertNewObjectForNamedEntity(Black.self, inManagedObjectContext: context)
                blackname.name = "test"
            }
            
        }
        try! context.save()
        CoreDataService.sharedCoreDataService.saveRootContext{
            print("store data succeed")
        }
    }
 
    static let sharedCatFriendsService = catFriend()
    
}