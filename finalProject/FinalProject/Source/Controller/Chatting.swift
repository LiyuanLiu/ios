//
//  Chatting.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/21/15.
//
//

import Foundation
import UIKit
class Chatting:UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let BackGroundImage:UIImageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width , self.view.frame.height))
        
        let image: UIImage = UIImage(named: "chatting")!
        
        BackGroundImage.image = image
        self.view.addSubview(BackGroundImage)
        
        youLabel.text = "You:"
        yourFriendLabel.text = "\(self.Friend_Name!):"//"Your Friend:"
        yourFriendMsg.text = ""
        yourMsg.text = ""
        self.view.bringSubviewToFront(yourFriendLabel)
        self.view.bringSubviewToFront(youLabel)
        self.view.bringSubviewToFront(sendContent)
        self.view.bringSubviewToFront(sendButton)
        
        self.view.bringSubviewToFront(yourMsg)
        self.view.bringSubviewToFront(yourFriendMsg)
        
        //let defaults = NSUserDefaults()
      //  let myname = defaults.stringForKey("userName")
        
        
       // let sendMsg = AppConfig.AddClientHead("chat", body: myname)
        
        client_server = TCPClient(addr:"127.0.0.1",port:7000)
        
        let (success,_) = client_server!.connect(timeout: 1)
        if success
        {
            //print("conneting to friend succeed")
            
            //let (succ,_) = client_server!.send(str: sendMsg)
            
           
            
            
        }
        
       
        
        
    }
    
    @IBAction func sendMessage(sender: UIButton) {
        
        let defaults = NSUserDefaults()
        let myname = defaults.stringForKey("userName")
        
        
        
        let msg = sendContent.text!
        let sendMsg = AppConfig.AddClientHead("chat", body: "|" + myname! + "|" + msg)
        
        let (_,_) = (client_server?.send(str: sendMsg))!
        
        let recvMsg = defaults.stringForKey(self.Friend_Name!)
        
        if(recvMsg != nil)
        {
            yourFriendMsg.text = recvMsg
            yourFriendMsg.reloadInputViews()
        }
        
        yourMsg.text = sendContent.text!
                
        sendContent.text = ""
        
        
        
        
        let data = client_server?.read(1024*10, timeout: 1)
        if let d = data
        {
            if let str = String(bytes: d, encoding: NSUTF8StringEncoding)
            {
                if str[0...2] == AppConfig.TransmissionAgreementConfiguration["chat"]
                {
                    let substring = str.substringFromIndex(3)
                    yourFriendMsg.text = substring
                    
                }
                else
                {
                    
                }
                
            }
        }
        
        
        
        
    }
    
    @IBOutlet weak var sendContent: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var yourFriendLabel: UILabel!
    
    @IBOutlet weak var yourFriendMsg: UILabel!
    
    @IBOutlet weak var youLabel: UILabel!
    
    @IBOutlet weak var yourMsg: UILabel!
    weak var viewDelegate:implmentAlertWindow?
    var AppConfig=TransmissionAgreement(first:"test")
    var client:TCPClient?
    var client_server:TCPClient?//connet to friend
    var flag:Int?//another client agree to chat ?
    var Friend_Name:String?
    
}