//
//  Detail.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/26/15.
//
//

import Foundation
import UIKit
import CoreData
import CoreDataService
class DetailViewController:UIViewController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName.text = username!
        
        let current_status = Status!
        if current_status == "Online"
        {
            Suggestion.text = "He is Online! You \ncan talk with him\n directly or add him!"
            Suggestion.numberOfLines = 0
        }
        else
        {
            Suggestion.text = "He is busy! You'd \nbetter talk with him later!"
            Suggestion.numberOfLines = 0

        }
        IPLabel.text = IP!
        PortNum.text = Port!
    }
    var Status:String?
    var username:String?
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var Suggestion: UILabel!
    
    @IBOutlet weak var IPLabel: UILabel!
    
    @IBOutlet weak var PortNum: UILabel!
    
    
    @IBAction func AddFriend(sender: UIButton) {
        
       // print("press add")
        
        let result = catFriend.sharedCatFriendsService.catBlack()
        try! result.performFetch()
        self.resultController = result
        
        
        
        //put it into local entity
        
        let firend_server = TCPClient(addr:"127.0.0.1",port:7000)
        let (success,_) = firend_server.connect(timeout: 1)
        if success
        {
            let defaults = NSUserDefaults()
            let myname = defaults.stringForKey("userName")
           let sendMsg = Appconfig.AddClientHead("add", body: myname)
            
           // print("send msg \(sendMsg) to client")
            
            let (succ,_) = firend_server.send(str: sendMsg)
            if succ
            {
                let data = firend_server.read(1024*10, timeout: 5)
                
                if let d = data
                {
                   // print("recv data \(data)")
                    if let str = String(bytes: d, encoding: NSUTF8StringEncoding)
                    {
                        //print("I am \(str)")
                        let head = str[0...2]
                        if head! == Appconfig.TransmissionAgreementConfiguration["agreeAdd"]
                        {
                           //print("another client agrees to add me")
                            //put it into local entity
                            let context = CoreDataService.sharedCoreDataService.mainQueueContext
                           
                            let friendName = NSEntityDescription.insertNewObjectForNamedEntity(Friends.self, inManagedObjectContext: context)
                            friendName.name = str.substringFromIndex(3)
                            
                            try! context.save()
                            
                          
                        }
                        else
                        {
                            UIAlertView(title: nil, message: "You cannot add him as your friend", delegate: nil, cancelButtonTitle: "OK").show()
                        }
                    }
                }
                else
                {
                    //print("recv nothing \(err)")
                }
            }
        }
    
  
    }
    
    
    @IBAction func AddBlackList(sender: UIButton) {
        
      let context = CoreDataService.sharedCoreDataService.mainQueueContext
        
        let blackName = NSEntityDescription.insertNewObjectForNamedEntity(Black.self, inManagedObjectContext: context)
        
        blackName.name = self.username!
        
        try! context.save()
        
        
    }
    
 
    let Appconfig = TransmissionAgreement(first: "first")
    var client:TCPClient?
    var IP:String?
    var Port:String?
    private var resultController: NSFetchedResultsController?
    
}