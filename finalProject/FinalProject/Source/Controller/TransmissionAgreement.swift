//
//  TCPConfiguration.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/14/15.
//
//

import Foundation
import UIKit



//my friend's list will be stored on local, each time when I login, I need to get his current IP and status via server
class TransmissionAgreement{
    
    init(first:String)
    {
        /*------------client send to server---------------*/
        //login and register//each time when it logins, it will send his friend's list to check their status and get their IP via server
        TransmissionAgreementConfiguration["login"] = "001"
        TransmissionAgreementConfiguration["register"] = "002"
        //logout, this msg will also be sent to other friends and server,receiver will show thie newest status
        TransmissionAgreementConfiguration["logout"] = "003"
        //check online list
        TransmissionAgreementConfiguration["check"] = "004"
        //add friend//send this msg to server to get his current IP and status,if it is offline, cannot add friends
        TransmissionAgreementConfiguration["add"] = "005"
        
        
        /*-------------client send to client(positive)---------------*/
        //changing status//also send to server
        TransmissionAgreementConfiguration["online"] = "051"
        TransmissionAgreementConfiguration["invisible"] = "052"
        TransmissionAgreementConfiguration["busy"] = "053"
        //send msg
        TransmissionAgreementConfiguration["chat"] = "054"//we don't need "add", when add a friend,just store,this chat means open the chatting scene
        TransmissionAgreementConfiguration["chatContent"] = "056"//send chatting content to it
        
        TransmissionAgreementConfiguration["agreeChat"] = "050"//agree to chat
        
        
        
        
        //his info in local entity.when chatting with him, start to connect with him
        //add friend//send to client//including self.username
        TransmissionAgreementConfiguration["add"] = "055"
        
        /*-------------client replies to client(negative)--------------*/
        TransmissionAgreementConfiguration["agreeAdd"] = "059"
        TransmissionAgreementConfiguration["refuse"] = "057"
        //if sender already in receiver's black list but he is trying to chatting, it will send "refuse" automatically
        
        //stop connection(because add black list)
        TransmissionAgreementConfiguration["stop"] = "058"
        
        
        /*------------server send to client-----------------*/
        //login error
        TransmissionAgreementConfiguration["errLoginUsernameNotExist"] = "041"
        TransmissionAgreementConfiguration["errLoginUsernameAlreadyOnline"] = "046"
        TransmissionAgreementConfiguration["errLoginPassword"] = "047"
        
        //register error
        TransmissionAgreementConfiguration["errRegisterUsernameExist"] = "042"
        
        //logout error
        TransmissionAgreementConfiguration["errLogout"] = "043"
        
        //add err,this one is not online
        TransmissionAgreementConfiguration["errAddOffline"] = "044"
        TransmissionAgreementConfiguration["errAddNotExist"] = "048"
        
        //replies with user list(online)
        TransmissionAgreementConfiguration["userList"] = "105"
        
        //login success
        TransmissionAgreementConfiguration["successLogin"] = "101"
        //register success
        TransmissionAgreementConfiguration["successRegister"] = "102"//the fourth and fifth digit will be his userID
        
        //logout success
        TransmissionAgreementConfiguration["successLogout"] = "103"
        
    }
    func AddClientHead(type:String,body:String?) -> String
    {
        if body != nil
        {
            return String(TransmissionAgreementConfiguration[type]!) + body!
        }
        else
        {
            return String(TransmissionAgreementConfiguration[type]!)
        }
        
    }
    //only login and register will receive the new userID,others will add userID
    func AddHead(type:String, body: String?) -> String
    {
        if((type == "login")||(type == "register"))
        {
            return String("--"+TransmissionAgreementConfiguration[type]!) + body!
        }
        else//others
        {
            let userID = NSUserDefaults()
            let id = userID.stringForKey("userID")
            if body != nil
            {
                return String(id!+TransmissionAgreementConfiguration[type]!) + body!
            }
            else 
            {
             //   print("id is \(id!)")
              //  print("type is \(type)")
                return String(id!+TransmissionAgreementConfiguration[type]!)
            }
        }
    }
    func SeperateHeadBody(content:String) -> (head:String,body:String)
    {
        let head = content[0...4]
        let body = content[5...content.length-1]
        
        return (head!,body!)
        
        
    }
    func StoreUserID(head:String)
    {
        let userID = NSUserDefaults()
       // print("head is \(head)")
       // let id1:String = head[3]
       // let id2:String = head[4]
        let id = head.substringFromIndex(3)
        
        userID.setObject(id, forKey: "userID")
        //userID is the last two digits
        
    }
    var TransmissionAgreementConfiguration = [String: String]()
}

/*----login, send his local friend's list to server. server replies with these friends's current status and IP(if they are online)
when the user logins, if also send "online" msg to all his friends after he got all online friends' IP
Add friends: send msg to server to get status and IP. Then send add msg to that client. If the receiver agrees, establish connection. When you want to chat with someone, just chatting with him if your local client code checks that it already in your stored data and his status is online(online means you guy already have a connection)
----------*/




