//
//  CheckList.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/16/15.
//
//

import Foundation
import UIKit
class CheckList: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    //MARK: - Tableview Delegate & Datasource
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        return userNumOnUserList!
        //return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell:UITableViewCell
        
        cell = tableView.dequeueReusableCellWithIdentifier("userOnlineCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = userList![indexPath.row*4]
        
        
        cell.detailTextLabel?.text = "Status:\(getStatus(userList![indexPath.row*4+1][2])!)"
        
        
        
        
        return cell

    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let sendMsg = AppConfig.AddHead("check", body: nil)
        let (success,_) = client!.send(str: sendMsg)
        
        if success
        {
            let data = client?.read(1024*10)
            if let d = data
            {
                if let str = String(bytes: d, encoding: NSUTF8StringEncoding)
                {
                    if str[0...2] == AppConfig.TransmissionAgreementConfiguration["userList"]
                    {
                        
                        let substring = str.substringFromIndex(3)
                        
                       // print("substring is \(substring)")
                        
                        userList = substring.split("|")
                        
                        userNumOnUserList = Int((userList?.last)!)
                        
                       // print("recv user list is \(str), num is \(userNumOnUserList!)")
                        
                        
                        
                    }
                }
                
            }
        }
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        
        switch segue.identifier
        {
        case .Some("detailSegue"):
            let indexPath = userListCell.indexPathForSelectedRow!
            let detailViewController = segue.destinationViewController as! DetailViewController
            
            detailViewController.Status = getStatus(userList![indexPath.row*4+1][2])!
            
            detailViewController.username = userList![indexPath.row*4]
            
            detailViewController.client = client
            
            detailViewController.IP = userList![indexPath.row*4+2]
            
            detailViewController.Port = userList![indexPath.row*4+3]
            
        default:
            super.prepareForSegue(segue, sender: sender)
        }
    }
    
    func getStatus(status:Character)->String?
    {
        var str:String?
        switch(status)
        {
        case "1":
            str = "Online"
        case "3":
            str = "Busy"
        
        default:
            str = "Online"
        }
        return str
    }
    
        
    @IBOutlet weak var userListCell: UITableView!
    
    var userList:[String]?
    var userNumOnUserList:Int?
    var AppConfig = TransmissionAgreement(first: "test")
    var client:TCPClient?
}