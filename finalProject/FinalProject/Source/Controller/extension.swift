//
//  extension.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/14/15.
//
//

import Foundation
import UIKit
extension String {
    var length: Int {
        return self.characters.count
    }
    subscript (i:Int) -> Character{
        return self[self.startIndex.advancedBy(i)]
    }
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    func split(delimiter:String?="", limit:Int=0)->[String]
    {
        let arr = self.componentsSeparatedByString((delimiter != nil ? delimiter! : ""))
        return Array(arr[0..<(limit > 0 ? min(limit, arr.count) : arr.count)])
    }
    
    subscript (r: Range<Int>) -> String? { //Optional String as return value
        get {
            let stringCount = self.characters.count as Int
            //Check for out of boundary condition
            if (stringCount < r.endIndex) || (stringCount < r.startIndex){
                return nil
            }
            let startIndex = self.startIndex.advancedBy(r.startIndex)
            
            let endIndex = self.startIndex.advancedBy(r.endIndex - r.startIndex)
            
            return self[Range(start: startIndex, end: endIndex)]
        }
    }
    func substringFromIndex(index: Int) -> String
    {
        if (index < 0 || index > self.characters.count)
        {
          //  print("index \(index) out of bounds")
            return ""
        }
        return self.substringFromIndex(self.startIndex.advancedBy(index))
    }
    func substringToIndex(index: Int) -> String
    {
        if (index < 0 || index > self.characters.count)
        {
           // print("index \(index) out of bounds")
            return ""
        }
        return self.substringToIndex(self.startIndex.advancedBy(index))
    }
   
       
}

