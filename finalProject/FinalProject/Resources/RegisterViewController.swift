//
//  RegisterViewController.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/5/15.
//
//

import Foundation
import UIKit

class RegisterViewController: UIViewController
{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let BackGroundImage:UIImageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width , self.view.frame.height))
        
        let image: UIImage = UIImage(named: "register.jpg")!
        
        BackGroundImage.image = image
        self.view.addSubview(BackGroundImage)
        
        
        usernameText.text = register_name
        passwordText.text = register_password
        
        usernameWarning.text = ""
        passwordWarning.text = ""
        repasswordWarning.text = ""
        
        self.view.bringSubviewToFront(usernameLabel)
        self.view.bringSubviewToFront(passwordLabel)
        self.view.bringSubviewToFront(usernameText)
        self.view.bringSubviewToFront(passwordText)
        
        self.view.bringSubviewToFront(re_passwordLabel)
        self.view.bringSubviewToFront(re_passwordText)
        
        self.view.bringSubviewToFront(Reset)
        self.view.bringSubviewToFront(Register)
        
        
        self.view.bringSubviewToFront(usernameWarning)
        self.view.bringSubviewToFront(passwordWarning)
        self.view.bringSubviewToFront(repasswordWarning)
        
        
       
        
    }
    
    @IBAction func ResetAll(sender: AnyObject) {
        
        let string = ""
        usernameText.text = string
        passwordText.text = string
        re_passwordText.text = string
        
        usernameWarning.text = string
        passwordWarning.text = string
        repasswordWarning.text = string
        
        
        self.usernameText.reloadInputViews()
        self.passwordText.reloadInputViews()
        self.re_passwordText.reloadInputViews()
        
        self.usernameWarning.reloadInputViews()
        self.passwordWarning.reloadInputViews()
        self.repasswordWarning.reloadInputViews()
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch(segue.identifier)
        {
        case .Some("RegularRegisterSegue"):
            let functionViewController = segue.destinationViewController as! FunctionsViewController
            functionViewController.client = client
            default:
            super.prepareForSegue(segue, sender: sender)
            
            
        }
    }
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "RegularRegisterSegue"
        {
            if usernameText.text != ""
            {
                if usernameText.text!.containsString("|")
                {
                    usernameWarning.text = "Username cannot contain '|' "
                    usernameWarning.reloadInputViews()
                    
                    passwordWarning.text = ""
                    passwordWarning.reloadInputViews()
                }
                else
                {
                    if passwordText.text != ""
                    {
                        let temp_string = passwordText.text
                        if temp_string?.length >= 4
                        {
                            usernameWarning.text = ""
                            usernameWarning.reloadInputViews()
                        
                            passwordWarning.text = ""
                            passwordWarning.reloadInputViews()
                        
                            if re_passwordText.text != ""
                            {
                                let temp_restring = re_passwordText.text
                                let isEqual = ( temp_string == temp_restring)
                                if isEqual
                                {
                                    //send to server
                                    
                                    let AppConfig = TransmissionAgreement(first:"first")
                                    let (success,_) = client.connect(timeout: 1)
                                    
                                    let encrypt = encryptMsg(send: passwordText.text!)
                                    let encrypt_pass = encrypt.encryptSendMsg()
                                    
                                    let content = usernameText.text! + "|" + encrypt_pass
                                    if success
                                    {
                                        let string = AppConfig.AddHead("register", body: content)
                                        let (success,_) = client.send(str: string)
                                        
                                     //   print("client register and send :\(string)")
                                        
                                        if success
                                        {
                                            
                                            let defaults = NSUserDefaults()
                                            defaults.setObject(usernameText.text!, forKey: "userName")
                                            
                                            
                                            let data = client.read(1024*10)
                                            if let d = data
                                            {
                                                if let str = String(bytes: d, encoding: NSUTF8StringEncoding)
                                                {
                                                  //  print("recv is \(str)")
                                                    let substr = str[0...2]
                                                    if substr == AppConfig.TransmissionAgreementConfiguration["successRegister"]//the last two digits will be his usrID
                                                    {
                                                        AppConfig.StoreUserID(str)
                                                        let defaults = NSUserDefaults()
                                                        defaults.setObject(usernameText.text!, forKey: "userName")
                                                        
                                                     //   print("register succeed")
                                                        
                                                        let acceptThread = AcceptThread()
                                                        acceptThread.start()
                                                        
                                                        
                                                        return true
                                                    }
                                                    else if str == AppConfig.TransmissionAgreementConfiguration["errRegisterUsernameExist"]
                                                    {
                                                        usernameWarning.text = "User name already exists"
                                                        usernameWarning.reloadInputViews()
                                                        passwordWarning.text = ""
                                                        passwordWarning.reloadInputViews()
                                                        repasswordWarning.text = ""
                                                        repasswordWarning.reloadInputViews()
                                                    
                                                        return false
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        return false
                                    }
                                
                                }
                                else
                                {
                                    usernameWarning.text = ""
                                    usernameWarning.reloadInputViews()
                                
                                    passwordWarning.text = "password is not same"
                                    repasswordWarning.text = "password is not same"
                                
                                    passwordWarning.reloadInputViews()
                                    repasswordWarning.reloadInputViews()
                                    return false
                                
                                }
                            }
                            else
                            {
                                usernameWarning.text = ""
                                usernameWarning.reloadInputViews()
                            
                                passwordWarning.text = ""
                                passwordWarning.reloadInputViews()
                            
                                repasswordWarning.text = "please input password twice"
                                repasswordWarning.reloadInputViews()
                                return false
                            
                            }
                        
                        }
                        else
                        {
                            usernameWarning.text = ""
                            usernameWarning.reloadInputViews()
                        
                            passwordWarning.text = "password size is less than 4"
                            passwordWarning.reloadInputViews()
                            return false
                        }
                    
                    
                    }
                    else
                    {
                        usernameWarning.text = ""
                        usernameWarning.reloadInputViews()
                    
                        passwordWarning.text = "please input password"
                        passwordWarning.reloadInputViews()
                        return false
                    
                    }
                }
            }
            else
            {
                usernameWarning.text = "please input username"
                usernameWarning.reloadInputViews()
                return false
            }

            
        }
        return false
    }
    
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var re_passwordLabel: UILabel!
    
    
    @IBOutlet weak var usernameText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var re_passwordText: UITextField!

    @IBOutlet weak var Reset: UIButton!
    
    
    @IBOutlet weak var Register: UIButton!
    
    
    @IBOutlet weak var usernameWarning: UILabel!
    
    @IBOutlet weak var passwordWarning: UILabel!
    
    
    @IBOutlet weak var repasswordWarning: UILabel!
    
    //var client: TCPClient = TCPClient(addr:"localhost",port:8889)
     var client: TCPClient = TCPClient(addr:"ix.cs.uoregon.edu",port:8889)
    var register_name: String?
    var register_password: String?
    var delegate:ClientReadThreadDelegate?
}












