//
//  RegularLogin.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/5/15.
//
//

import Foundation
import UIKit
import CoreData
import CoreDataService

class RegularLogin:UIViewController,/*UITableViewController*/ UITableViewDataSource,UITableViewDelegate
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if resultsController!.sections![section].numberOfObjects > 1
        {
            
            return resultsController!.sections![section].numberOfObjects - 1

            
        }
        else
        {
            flag = 1
            return 1
        }
        
    
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell
        let friend = resultsController!.objectAtIndexPath(indexPath) as! Friends
        cell = tableView.dequeueReusableCellWithIdentifier("friend", forIndexPath: indexPath)
        
        
        
        
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.clearColor()
        
        if flag == 1
        {
            cell.textLabel!.text = "You don't have friends now"
           // cell.detailTextLabel!.text = "Nothing"
        }
        else
        {
            cell.textLabel!.text = friend.name
           // cell.detailTextLabel!.text = "Friends"
        }
        


        return cell
        
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        let resultController = /*try?*/ catFriend.sharedCatFriendsService.catFriends()
        try! resultController.performFetch()
        self.resultsController = resultController
        
        
     
        
        
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch(segue.identifier)
        {
            /*case .Some("functionSegue"):
                let functionViewController = segue.destinationViewController as! FunctionsViewController
                functionViewController.client = client!*/
            case .Some("chattingSegue"):
                let chattingViewController = segue.destinationViewController as! Chatting
                chattingViewController.client = client!
                let index_path = friendListCell.indexPathForSelectedRow!
            
            let itemSelected = friendListCell.cellForRowAtIndexPath(index_path)
            
            chattingViewController.Friend_Name = itemSelected?.textLabel?.text!
            
           // print("------\(chattingViewController.Friend_Name!)")
                
        default:
            super.prepareForSegue(segue, sender: sender)
        }
        
        
        
    }
    
    
    var flag:Int?
    var client:TCPClient?
    var cellNameSelected:String?
    private var resultsController: NSFetchedResultsController?
 
    @IBOutlet weak var friendListCell: UITableView!
}


















