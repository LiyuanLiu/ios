//
//  FunctionsViewController.swift
//  FinalProject
//
//  Created by Liyuan Liu on 11/16/15.
//
//

import Foundation
import AVFoundation
import AssetsLibrary
import MobileCoreServices
import UIKit
class FunctionsViewController:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    var myImageView:UIImageView = UIImageView()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.myImageView = createImageView()
        
        
        self.view.addSubview(self.myImageView)
        
        self.view.bringSubviewToFront(basicFunction)
        self.view.bringSubviewToFront(checkButton)
        self.view.bringSubviewToFront(logoutButton)
        self.view.bringSubviewToFront(blackButton)
        self.view.bringSubviewToFront(changeStatus)
        self.view.bringSubviewToFront(onlineButton)
        self.view.bringSubviewToFront(busyButton)
        self.view.bringSubviewToFront(invisibleButton)
        
        self.view.bringSubviewToFront(takephoto)
        self.view.bringSubviewToFront(pick)
        
        
        CurrentStatus.text = "Current Status"
        StatusLabel.text = "Online"
        changeBack.text = "Change Background"
        
        self.view.bringSubviewToFront(CurrentStatus)
        self.view.bringSubviewToFront(StatusLabel)
        self.view.bringSubviewToFront(changeBack)
        
        
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       switch segue.identifier
       {
      
       case .Some("checkSegue"):
        let checklistViewController = segue.destinationViewController as! CheckList
        checklistViewController.client = client
        
       // print("check")
        
        
       case .Some("friendSegue"):
        let friendList = segue.destinationViewController as! RegularLogin
        
        
        friendList.client = client
        
        
       case .Some("blackSegue"):
        print("black")
        
       default:
        break
        
       }
    }
    
    func createImageView() -> UIImageView {
        
        let imageView: UIImageView = UIImageView(frame: CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64))
        let image: UIImage = UIImage(named: "functions.jpg")!
        imageView.image = image
        return imageView
    }
    
    
    @IBAction func onlineStatus(sender: AnyObject) {
        //send change status to online to server and firends
        
        let sendMsg = AppConfig.AddHead("online", body: nil)
        let (_,_) = client!.send(str: sendMsg)
        
        
        StatusLabel.text = "Online"
        StatusLabel.reloadInputViews()
        //send to friends
        
    }
    
    @IBAction func busyStatus(sender: UIButton) {
        //send change status to online to server and firends
        
        let sendMsg = AppConfig.AddHead("busy", body: nil)
        let (_,_) = client!.send(str: sendMsg)
        
        StatusLabel.text = "Busy"
        StatusLabel.reloadInputViews()
        
        //send to friends
    }
    
    @IBAction func invisibleStatus(sender: UIButton) {
        //send change status to online to server and firends
        
        let sendMsg = AppConfig.AddHead("invisible", body: nil)
        let (_,_) = client!.send(str: sendMsg)
        
        StatusLabel.text = "Invisible"
        StatusLabel.reloadInputViews()
        //send to friends
    }
    
    
    @IBAction func logoutAccount(sender: UIButton) {
        
        //send logout to server
        let sendMsg = AppConfig.AddHead("logout", body: nil)
      //  print("send msg \(sendMsg) to server")
        
        let(_,_) = client!.send(str: sendMsg)
        
        //send to friends
    }
    
    
    @IBOutlet weak var basicFunction: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    
    
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var blackButton: UIButton!
    
    @IBOutlet weak var changeStatus: UILabel!
    
    @IBOutlet weak var onlineButton: UIButton!
    
    @IBOutlet weak var busyButton: UIButton!
    
    @IBOutlet weak var invisibleButton: UIButton!
    
    @IBOutlet weak var StatusLabel: UILabel!
    
    @IBOutlet weak var CurrentStatus: UILabel!
    
    
    @IBOutlet weak var takephoto: UIButton!
    
    @IBOutlet weak var pick: UIButton!
    
    @IBOutlet weak var changeBack: UILabel!
    
    @IBAction func takePhoto(sender: UIButton) {
        if isCameraAvailable() && doesCameraSupportTakingPhotos()
        {
            let controller = UIImagePickerController()
            controller.view.backgroundColor = UIColor.whiteColor()
            controller.sourceType = UIImagePickerControllerSourceType.Camera
            controller.mediaTypes = [kUTTypeImage as String]
            controller.allowsEditing = true
            controller.delegate = self
            
            let sysVersion = (UIDevice.currentDevice().systemVersion as NSString).floatValue
            if sysVersion >= 8.0{
                controller.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            }
            
            if !self.isAuthorized(){
                if sysVersion >= 8.0 {
                    let alertVC = UIAlertController(title: nil, message: "deny", preferredStyle: UIAlertControllerStyle.Alert)
                    let openIt = UIAlertAction(title: "open", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                        UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
                    })
                    alertVC.addAction(openIt)
                    self.presentViewController(alertVC, animated: true, completion: nil)
                }else{
                    let alertVC = UIAlertController(title: "notice", message: "please set privacy", preferredStyle: UIAlertControllerStyle.Alert)
                    self.presentViewController(alertVC, animated: true, completion: nil)
                }
            }else{
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }else{
            print("doesn't support taking photo")
            
        }

        
        
    }
    
    @IBAction func pickLibrary(sender: UIButton) {
        if self.isPhotoLibraryAvailable(){
            let controller = UIImagePickerController()
            controller.view.backgroundColor = UIColor.whiteColor()
            controller.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            var mediaTypes = [String]()
            if self.canUserPickPhotosFromPhotoLibrary()
            {
                mediaTypes.append(kUTTypeImage as String)
                
            }
            controller.allowsEditing = true
            controller.mediaTypes = mediaTypes
            if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0{
                controller.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            }
            controller.delegate = self
            self.presentViewController(controller, animated: true, completion: nil)
            
        }
        else
        {
            print("doesn't support")
        }
        
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        if mediaType.isEqualToString(kUTTypeImage as String) {
            let theImage : UIImage!
            if picker.allowsEditing{
                theImage = info[UIImagePickerControllerEditedImage] as! UIImage
            }else{
                theImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            }
            
            self.myImageView.image=theImage 
        }else{
            
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true) { () -> Void in
            
            print("Cancel")
        }
    }
    
    
    
    func isCameraAvailable() -> Bool{
        return UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
    }
    
    
    func isFrontCameraAvailable() -> Bool{
        return UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Front)
    }
    
    
    func isRearCameraAvailable() -> Bool{
        return UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Rear)
    }
    
    
    
    func cameraSupportsMedia(paramMediaType:NSString, sourceType:UIImagePickerControllerSourceType) -> Bool {
        var result = false
        if paramMediaType.length == 0 {
            return false
        }
        let availableMediaTypes = NSArray(array: UIImagePickerController.availableMediaTypesForSourceType(sourceType)!)
        availableMediaTypes.enumerateObjectsUsingBlock { (obj:AnyObject, idx:NSInteger, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
            let type = obj as! NSString
            if type.isEqualToString(paramMediaType as String) {
                result = true
                stop.memory = true
            }
        }
        return result
    }
    
    
    func isAuthorized() -> Bool{
        let mediaType = AVMediaTypeVideo
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(mediaType)
        if authStatus == AVAuthorizationStatus.Restricted ||
            authStatus == AVAuthorizationStatus.Denied{
                return false
        }
        return true
    }
    
   
    func doesCameraSupportShootingVides() -> Bool{
        return self.cameraSupportsMedia(kUTTypeMovie, sourceType: UIImagePickerControllerSourceType.Camera)
    }
   
    func doesCameraSupportTakingPhotos() -> Bool{
        return self.cameraSupportsMedia(kUTTypeImage, sourceType: UIImagePickerControllerSourceType.Camera)
    }
    
    
   
    func isPhotoLibraryAvailable() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
    }
  
    func canUserPickPhotosFromPhotoLibrary() -> Bool {
        return self.cameraSupportsMedia(kUTTypeImage, sourceType: UIImagePickerControllerSourceType.PhotoLibrary)
    }
    

    
    
    var AppConfig=TransmissionAgreement(first:"test")
    var client:TCPClient?
}