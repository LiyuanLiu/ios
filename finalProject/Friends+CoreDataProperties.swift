//
//  Friends+CoreDataProperties.swift
//  FinalProject
//
//  Created by Liyuan Liu on 12/5/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Friends {

    @NSManaged var name: String?
    //@NSManaged var relation: NSNumber?

}
