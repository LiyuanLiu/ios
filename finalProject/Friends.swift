//
//  Friends.swift
//  FinalProject
//
//  Created by Liyuan Liu on 12/5/15.
//
//

import Foundation
import CoreData
import CoreDataService

class Friends: NSManagedObject, NamedEntity {
    static var entityName: String { return "Friends" }
}
