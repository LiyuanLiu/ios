//
//  workProfileViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/9/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit
class workProfileViewController:UIViewController,UITextFieldDelegate
{
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.hidden = true
        fromText.delegate = self
        endText.delegate = self
        companyText.delegate = self
        positionText.delegate = self
        
        datePicker.maximumDate = NSDate()
        endText.text = nil
        fromText.text = nil
        
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        whichDateText = textField
        
        if((textField == companyText)||(textField == positionText))
        {
            datePicker.hidden = true
        }
        else
        {
            datePicker.hidden = false
        }
      
        return true
    }
    
    
    

    
    @IBAction func datePicker(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        enddate = datePicker.date
        let date = dateFormatter.stringFromDate(datePicker.date)
        
        if(whichDateText == fromText)
        {
            fromText.text = date
            fromText.reloadInputViews()
        }
        else
        {
            endText.text = date
            endText.reloadInputViews()
        }
       // datePicker.hidden = true
        
    }
    
    var whichDateText:UITextField?
    var enddate:NSDate?
    var fromdate:NSDate?
    
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var companyText: UITextField!
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var positionText: UITextField!
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var fromText: UITextField!
    
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var endText: UITextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
       
    
}