//
//  onePeriodViewController.swift
//  Status
//
//  Created by Liyuan Liu on 3/7/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit

class onePeriodViewController: UIViewController,UITextFieldDelegate/*,UIPickerViewDelegate,UIPickerViewDataSource*/ {

    override func viewDidLoad() {
        super.viewDidLoad()

        bringObjectToFront()
        datePicker.hidden = true
        startTimeText.delegate = self
        endTimeText.delegate = self
        periodNameText.delegate = self
        descriptionText.delegate = self
     //   whichPolicyPicker.delegate = self
        
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
    }*/
    
    @IBAction func saveEdits(sender: AnyObject) {
        
        //send new period to server
    }
    
    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(periodNameLabel)
        self.view.bringSubviewToFront(periodNameText)
        
        self.view.bringSubviewToFront(startTimeLabel)
        self.view.bringSubviewToFront(startTimeText)
        
        self.view.bringSubviewToFront(endTimeLabel)
        self.view.bringSubviewToFront(endTimeText)
        
        self.view.bringSubviewToFront(whichPolicyLabel)
        self.view.bringSubviewToFront(whichPolicyPicker)
        
        self.view.bringSubviewToFront(descriptionLabel)
        self.view.bringSubviewToFront(descriptionText)
        
        self.view.bringSubviewToFront(datePicker)
    }

    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if((textField == periodNameText)||(textField == descriptionText))
        {
            datePicker.hidden = true
        }
        else
        {
            datePicker.hidden = false
        }
        whichText = textField
        return true
    }
    
    @IBAction func datePick(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        enddate = datePicker.date
        let date = dateFormatter.stringFromDate(datePicker.date)
        
        if(whichText == startTimeText)
        {
            startTimeText.text = date
            startTimeText.reloadInputViews()
        }
        else
        {
            endTimeText.text = date
            endTimeText.reloadInputViews()
        }
        
    }
    
    
    @IBOutlet weak var periodNameLabel: UILabel!
    @IBOutlet weak var periodNameText: UITextField!
    
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var startTimeText: UITextField!
    
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var endTimeText: UITextField!
    
    @IBOutlet weak var whichPolicyLabel: UILabel!
    @IBOutlet weak var whichPolicyPicker: UIPickerView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var whichText:UITextField?
    var enddate:NSDate?
    var startdate:NSDate?
}










