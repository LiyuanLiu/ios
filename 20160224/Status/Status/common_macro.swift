//
//  common_macro.swift
//  Status
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation

public enum LENGTH_MACRO:Int
{
    case REQUEST_TYPE_LENGTH_MAX
    case REPLY_TYPE_LENGTH_MAX
    case NOTICE_TYPE_LENGTH_MAX
    case ANSWER_TYPE_LENGTH_MAX
    case LENGTH_STRING_MAX //When send the strin of length, the max size of the string of length
    case USERNAME_MAX
    case PASSWORD_MAX
    case EMAIL_ADDR_MAX
    case CUSTOMIZED_STATUS_MAX
    case CHATTING_MSG_MAX
    var REQUEST_TYPE_LENGTH_MAX:Int
    {
        get {
            switch(self){
            case REQUEST_TYPE_LENGTH_MAX,REPLY_TYPE_LENGTH_MAX,NOTICE_TYPE_LENGTH_MAX,ANSWER_TYPE_LENGTH_MAX:
                return 4
            case LENGTH_STRING_MAX:
                return 10
            case USERNAME_MAX:
                return 50
            case PASSWORD_MAX:
                return 20
            case EMAIL_ADDR_MAX:
                return 100
            case CUSTOMIZED_STATUS_MAX:
                return 150
            case CHATTING_MSG_MAX:
                return 1024
            
                
            }
        }
    
    }
    
}

public enum STATUS_USER:String
{
    case AVALIABLE =         "300"
    case NOT_AVALIABLE =     "301"
    case BUSY =              "302"
    case CUSTOMIZED_STATUS = "400"
    var description:String{
        get{
            return self.rawValue
        }
    }
}







