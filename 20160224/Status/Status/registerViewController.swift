//
//  registerViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/2/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit
class registerViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        self.view.bringSubviewToFront(usernameText)
        self.view.bringSubviewToFront(usernameLabel)
        
        self.view.bringSubviewToFront(passwordLabel)
        self.view.bringSubviewToFront(passwordText)
        
        self.view.bringSubviewToFront(reenterLabel)
        self.view.bringSubviewToFront(reenterText)
        
        self.view.bringSubviewToFront(emailLabel)
        self.view.bringSubviewToFront(emailText)
        
        self.view.bringSubviewToFront(resetButton)
        self.view.bringSubviewToFront(registerButton)
        
        self.view.bringSubviewToFront(aboutStatusButton)
        
        self.view.bringSubviewToFront(usernameWarningLabel)
        self.view.bringSubviewToFront(passwordWarningLabel)
        self.view.bringSubviewToFront(reenterWarningLabel)
        self.view.bringSubviewToFront(emailWarningLabel)
        
        
    }
    
  /*  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier
        {
        case .Some("registerNewSegue"):
            let userInterfaceViewController = segue.destinationViewController as! userViewController
            userInterfaceViewController.username = usernameText!.text
            
        default:
            break
        }
    }*/

    @IBAction func ResetAll(sender: AnyObject) {
        
        usernameText.text = ""
        passwordText.text = ""
        reenterText.text = ""
        emailText.text = ""
        
        usernameWarningLabel.text = ""
        passwordWarningLabel.text = ""
        reenterWarningLabel.text = ""
        emailWarningLabel.text = ""
        
        self.usernameText.reloadInputViews()
        self.passwordText.reloadInputViews()
        self.reenterText.reloadInputViews()
        self.emailText.reloadInputViews()
        
        self.usernameWarningLabel.reloadInputViews()
        self.passwordWarningLabel.reloadInputViews()
        self.reenterWarningLabel.reloadInputViews()
        self.emailWarningLabel.reloadInputViews()
        
        
        
        
    }
    
    
    
    @IBOutlet weak var usernameText: UITextField!

    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var reenterText: UITextField!
    
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var aboutStatusButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var reenterLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var usernameWarningLabel: UILabel!
    
    @IBOutlet weak var passwordWarningLabel: UILabel!
    
    @IBOutlet weak var reenterWarningLabel: UILabel!
    
    @IBOutlet weak var emailWarningLabel: UILabel!
    
    
}













