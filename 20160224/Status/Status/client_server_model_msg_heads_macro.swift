//
//  client_server_model_msg_heads_macro.swift
//  Status
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

public enum TransRule:Int
{
    
        //------------client send to server(positively)------------------------------------------------
        case REGISTER =         10
        case LOGIN =            20
        case LOGOUT =           30
        
        //---send filtering policy---------------------
        case UPDATE_POLICY =    40
        
        //--send my schedule --------------------------
        case UPDATE_SCHEDULE =  50
        
        //----send my profile--------------------------
        case UPDATE_PROFILE =   55
        
        //------search friends-------------------------
        case SEARCH_FRIEND =    60
        
        //--------add friends--------------------------
        case ADD_FRIEND =       65
        
        //--------delete friends-----------------------
        case DELETE_FRIEND =    70
        
        //---------chatting----------------------------
        case CHAT =             75
        
        //------online automatically-------------------
        //client will send update msg to server, otherwise server will view client as logout due to client crash
        case ONLINE_UPDATE =    80
        
        //-------------server replies to client(negatively)--------------------------------------------
        //----succeed-------------------------------------------------------------
        case REGISTER_SUCCEED =                 110
        case LOGIN_SUCCEED =                    120
        case LOGOUT_SUCCEED =                   130
        
        case UPDATE_PLOICY_SUCCEED =            140
        case UPDATE_SCHEDULE_SUCCEED =          150
        case UPDATE_PROFILE_SUCCEED =           155
        case SEARCH_RESULT =                    160
        case ADD_SUCCEED =                      165
        case DELETE_FRIEND_SUCCEED =            170
        case CHAT_SUCCEED =                     175
        case ONLINE_UPDATE_SUCCEED =            180
        
        //------failed------------------------------------------------------------
        //----register failed---------------------------------------
        case REGISTER_FAILED_USERNAME_EXIST =   -10
        
        //------login failed----------------------------------------
        case LOGIN_FAILED_USERNAME_ONLINE =     -20
        case LOGIN_FAILED_USERNAME_NOT_EXIST =  -21
        case LOGIN_FAILED_PASSWORD_ERROR =      -22
        
        //-------logout failed--------------------------------------
        case LOGOUT_FAILED_NOT_ONLINE =         -30
        case LOGOUT_FAILED_NO_USERNAME =        -31
        
        
        /*
        1,limit someone can search my account just by my account name
        2,group friends in different groups, some groups can see my one status, some groups can see my
        another status
        3,
        */
        //----update policy failed-----------------------------------
        case UPDATE_POLICY_FAILED =             -40
        
        /* 1, we can have different versions of schedule, for different groups
        2,we can have different versions of schedule for different days in one week
        3,we can set detailed schedule for each day, and also set a rough schedule
        for a long time(like will be very busy in Jan)
        */
        //-----update schedule failed--------------------------------
        case UPDATE_SCHEDULE_FAILED =           -50
        
        //------update profile failed--------------------------------
        case UPDATE_PROFILE_FAILED =            -55
        
        //------search friends failed--------------------------------
        case SEARCH_FRIEND_NOT_EXIST =          -60
        
        //------add friends failed-----------------------------------
        case ADD_FAILED_REJECT =                -65
        
        //------delete friends failed--------------------------------
        case DELETE_FRIEND_FAILED =             -70
        
        //------send chat msg failed---------------------------------
        case CHAT_LOGOUT_FAILED =               -75
        
        //-------online update failed--------------------------------
        case ONLINE_UPDATE_FAILED =             -80
        
        //-----------------server sends to client(positively)------------------------------------------------
        //--------server tells client that anther wants to add him-----------
        case ADD_YOU =                          85
        //--------server tells client that another sends him chatting msg----
        case CHAT_TO_YOU =                      90
        
        
        
        //-----------------client replies to server(negatively)----------------------------------------------
        case IGNORE_ADD_REQUEST =               566
        case REJECT_ADD_REQUEST =               567
   
    
}

/*

class TransRule{
    init(first:String)
    {
        //------------client send to server(positively)------------------------------------------------
        ClientToServerPos["REGISTER"] =         10
        ClientToServerPos["LOGIN"] =            20
        ClientToServerPos["LOGOUT"] =           30
        
        //---send filtering policy---------------------
        ClientToServerPos["UPDATE_POLICY"] =    40
        
        //--send my schedule --------------------------
        ClientToServerPos["UPDATE_SCHEDULE"] =  50
        
        //----send my profile--------------------------
        ClientToServerPos["UPDATE_PROFILE"] =   55
        
        //------search friends-------------------------
        ClientToServerPos["SEARCH_FRIEND"] =    60
        
        //--------add friends--------------------------
        ClientToServerPos["ADD_FRIEND"] =       65
        
        //--------delete friends-----------------------
        ClientToServerPos["DELETE_FRIEND"] =    70
        
        //---------chatting----------------------------
        ClientToServerPos["CHAT"] =             75
        
        //------online automatically-------------------
        //client will send update msg to server, otherwise server will view client as logout due to client crash
        ClientToServerPos["ONLINE_UPDATE"] =    80
        
        //-------------server replies to client(negatively)--------------------------------------------
        //----succeed-------------------------------------------------------------
        ServerToClientNeg["REGISTER_SUCCEED"] =                 110
        ServerToClientNeg["LOGIN_SUCCEED"] =                    120
        ServerToClientNeg["LOGOUT_SUCCEED"] =                   130
        
        ServerToClientNeg["UPDATE_PLOICY_SUCCEED"] =            140
        ServerToClientNeg["UPDATE_SCHEDULE_SUCCEED"] =          150
        ServerToClientNeg["UPDATE_PROFILE_SUCCEED"] =           155
        ServerToClientNeg["SEARCH_RESULT"] =                    160
        ServerToClientNeg["ADD_SUCCEED"] =                      165
        ServerToClientNeg["DELETE_FRIEND_SUCCEED"] =            170
        ServerToClientNeg["CHAT_SUCCEED"] =                             175
        ServerToClientNeg["ONLINE_UPDATE_SUCCEED"] =            180
        
        //------failed------------------------------------------------------------
        //----register failed---------------------------------------
        ServerToClientNeg["REGISTER_FAILED_USERNAME_EXIST"] =   -10
        
        //------login failed----------------------------------------
        ServerToClientNeg["LOGIN_FAILED_USERNAME_ONLINE"] =     -20
        ServerToClientNeg["LOGIN_FAILED_USERNAME_NOT_EXIST"] =  -21
        ServerToClientNeg["LOGIN_FAILED_PASSWORD_ERROR"] =      -22
        
        //-------logout failed--------------------------------------
        ServerToClientNeg["LOGOUT_FAILED_NOT_ONLINE"] =         -30
        ServerToClientNeg["LOGOUT_FAILED_NO_USERNAME"] =        -31
        
        
        /*
        1,limit someone can search my account just by my account name
        2,group friends in different groups, some groups can see my one status, some groups can see my
        another status
        3,
        */
        //----update policy failed----------------------------------
        ServerToClientNeg["UPDATE_POLICY_FAILED"] =             -40
        
        /* 1, we can have different versions of schedule, for different groups
        2,we can have different versions of schedule for different days in one week
        3,we can set detailed schedule for each day, and also set a rough schedule
        for a long time(like will be very busy in Jan)
        */
        //-----update schedule failed------------------------------
        ServerToClientNeg["UPDATE_SCHEDULE_FAILED"] =           -50
        
        //------update profile failed------------------------------
        ServerToClientNeg["UPDATE_PROFILE_FAILED"] =            -55
        
        //------search friends failed------------------------------
        ServerToClientNeg["SEARCH_FRIEND_NOT_EXIST"] =          -60
        
        //------add friends failed---------------------------------
        ServerToClientNeg["ADD_FAILED_REJECT"] =                -65
        
        //------delete friends failed------------------------------
        ServerToClientNeg["DELETE_FRIEND_FAILED"] =             -70
        
        //------send chat msg failed-------------------------------
        ServerToClientNeg["CHAT_LOGOUT_FAILED"] =               -75
        
        //-------online update failed------------------------------
        ServerToClientNeg["ONLINE_UPDATE_FAILED"] =             -80
        
        //-----------------server sends to client(positively)------------------------------------------------
        //--------server tells client that anther wants to add him-----------
        ServerToClientPos["ADD_YOU"] =                          85
        //--------server tells client that another sends him chatting msg----
        ServerToClientPos["CHAT_TO_YOU"] =                      90
        
        
        
        //-----------------client replies to server(negatively)----------------------------------------------
        ClientToServerNeg["IGNORE_ADD_REQUEST"] =               566
        ClientToServerNeg["REJECT_ADD_REQUEST"] =               567
        
        
    }
    
    //client sends to server postively
    var ClientToServerPos = [String:Int]()
    
    //server replies to client(negatively)
    var ServerToClientNeg = [String:Int]()
    
    //server sends to client posivitely
    var ServerToClientPos = [String:Int]()
    
    //client replies to server(negatively)
    var ClientToServerNeg = [String:Int]()
    
    
}*/



















