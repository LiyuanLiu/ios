//
//  profileViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/3/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

class profileViewController:UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
    
        let cell: UITableViewCell
        
        cell = tableView.dequeueReusableCellWithIdentifier("info", forIndexPath: indexPath)
        
        switch indexPath.row
        {
        case 0:
            cell.textLabel?.text = "Basic Information"
        case 1:
            cell.textLabel?.text = "Education Information"
        case 2:
            cell.textLabel?.text = "Working Information"
            
        default:
            break
        }
        return cell
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row
        {
        case 0:
            self.performSegueWithIdentifier("basicProfileSegue", sender: self)
        case 1:
            self.performSegueWithIdentifier("eduProfileSegue", sender: self)
        case 2:
            self.performSegueWithIdentifier("workProfileSegue", sender: self)
        default:
            break
            
        }

        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
       
        
        // Do any additional setup after loading the view.
    }
    
}