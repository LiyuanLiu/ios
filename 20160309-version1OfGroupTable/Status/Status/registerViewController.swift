//
//  registerViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/2/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import UIKit
class registerViewController: UIViewController,UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        usernameText.delegate = self
        passwordText.delegate = self
        emailText.delegate = self
        reenterText.delegate = self
        
        usernameWarningLabel.text = ""
        passwordWarningLabel.text = ""
        reenterWarningLabel.text = ""
        emailWarningLabel.text = ""
        
        bringObjectToFront()
        
        
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        
        let temp_string = String(textField.text!)
        
        switch textField
        {
        case usernameText:
            
            if(usernameText.text != nil)
            {
                if(temp_string.containsString(String(client_Init_Value.INIT_VALUE.description))||(temp_string.containsString(String(client_Init_Value.SEPERATOR.description))))
                {
                    usernameWarningLabel.text = "username cannot contain special charactors"
                    usernameWarningLabel.reloadInputViews()
                    
                }
            }
        case passwordText:
            if(passwordText.text != nil)
            {
                if(temp_string.containsString(String(client_Init_Value.INIT_VALUE.description))||(temp_string.containsString(String(client_Init_Value.SEPERATOR.description))))
                {
                    passwordWarningLabel.text = "password cannot contain special charactors"
                    passwordWarningLabel.reloadInputViews()
                    
                }
            }
        case emailText:
            if(emailText.text != nil)
            {
                if(temp_string.containsString(String(client_Init_Value.INIT_VALUE.description))||(temp_string.containsString(String(client_Init_Value.SEPERATOR.description))))
                {
                    emailWarningLabel.text = "email addr cannot contain special charactors"
                    emailWarningLabel.reloadInputViews()
                    
                }
                if(temp_string.length>=LENGTH_MACRO.EMAIL_ADDR_MAX.REQUEST_TYPE_LENGTH_MAX)
                {
                    emailWarningLabel.text = "length of email addr need be less than 50"
                    emailWarningLabel.reloadInputViews()
                }
            }
            
        default:
            break
        }
        
        return true
    }
    
    func bringObjectToFront()
    {
        self.view.bringSubviewToFront(usernameText)
        self.view.bringSubviewToFront(usernameLabel)
        
        self.view.bringSubviewToFront(passwordLabel)
        self.view.bringSubviewToFront(passwordText)
        
        self.view.bringSubviewToFront(reenterLabel)
        self.view.bringSubviewToFront(reenterText)
        
        self.view.bringSubviewToFront(emailLabel)
        self.view.bringSubviewToFront(emailText)
        
        self.view.bringSubviewToFront(resetButton)
        self.view.bringSubviewToFront(registerButton)
        
        self.view.bringSubviewToFront(aboutStatusButton)
        
        self.view.bringSubviewToFront(usernameWarningLabel)
        self.view.bringSubviewToFront(passwordWarningLabel)
        self.view.bringSubviewToFront(reenterWarningLabel)
        self.view.bringSubviewToFront(emailWarningLabel)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier
        {
        case .Some("registerNewSegue"):
            let tabbar = segue.destinationViewController as! UITabBarController
            
            let navController = tabbar.viewControllers![0] as! UINavigationController
            
            let destViewController = navController.viewControllers[0] as! userViewController
            
            destViewController.username = usernameText!.text

            
        default:
            break
        }
    }

    @IBAction func ResetAll(sender: AnyObject) {
        
        if(username == nil)
        {
            usernameText.text = ""
            
        }
        else
        {
            usernameText.text = username
        }
        if(password == nil)
        {
            passwordText.text = ""
        }
        else
        {
            passwordText.text = password
        }
        
        
        reenterText.text = ""
        emailText.text = ""
        
        usernameWarningLabel.text = ""
        passwordWarningLabel.text = ""
        reenterWarningLabel.text = ""
        emailWarningLabel.text = ""
        
        self.usernameText.reloadInputViews()
        self.passwordText.reloadInputViews()
        self.reenterText.reloadInputViews()
        self.emailText.reloadInputViews()
        
        self.usernameWarningLabel.reloadInputViews()
        self.passwordWarningLabel.reloadInputViews()
        self.reenterWarningLabel.reloadInputViews()
        self.emailWarningLabel.reloadInputViews()
        
        
        
        
    }
    
    @IBAction func register(sender: AnyObject) {
        
        //send to server
    }
    
    
    
    var username:String?
    var password:String?
    
    
    @IBOutlet weak var usernameText: UITextField!

    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var reenterText: UITextField!
    
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var aboutStatusButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var reenterLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var usernameWarningLabel: UILabel!
    
    @IBOutlet weak var passwordWarningLabel: UILabel!
    
    @IBOutlet weak var reenterWarningLabel: UILabel!
    
    @IBOutlet weak var emailWarningLabel: UILabel!
    
    
}













