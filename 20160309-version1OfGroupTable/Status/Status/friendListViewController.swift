//
//  friendListViewController.swift
//  Status
//
//  Created by Liyuan Liu on 2/11/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

class friendListViewController:UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    let groupData = [
        groupTreeModel(label: "atest1", image: nil, children: [
            groupTreeModel(label: "subtest1"),
            groupTreeModel(label: "subtest2")
            ], isCollapsed: true),
        groupTreeModel(label: "atest2", image: nil, children: [
            groupTreeModel(label: "subtest1"),
            groupTreeModel(label: "subtest2")
            ], isCollapsed: true),
        
    ]
    var displayedRows:[groupTreeModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayedRows = groupData
                
        // Do any additional setup after loading the view.
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedRows.count
        //return 2
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("groupFriendCell", forIndexPath: indexPath)
        let viewModel = displayedRows[indexPath.row]
        cell.textLabel!.text = viewModel.rootLabel
        //cell.textLabel?.text = "test"
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let viewModel = displayedRows[indexPath.row]
        if viewModel.children.count > 0 {
            let range = indexPath.row+1...indexPath.row+viewModel.children.count
            let indexPaths = range.map{return NSIndexPath(forRow: $0, inSection: indexPath.section)}
            tableView.beginUpdates()
            if viewModel.isCollapsed {
                displayedRows.insertContentsOf(viewModel.children, at: indexPath.row+1)
                
                print("\(displayedRows.count)")
                print("\(displayedRows[indexPath.row+1].rootLabel)")
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            } else {
                displayedRows.removeRange(range)
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            }
            tableView.endUpdates()
        }
        viewModel.isCollapsed = !viewModel.isCollapsed
    }
    
    
    
    
}









