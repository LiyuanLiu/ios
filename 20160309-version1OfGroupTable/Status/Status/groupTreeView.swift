//
//  groupTreeView.swift
//  Status
//
//  Created by Liyuan Liu on 3/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

import Foundation
import UIKit

class groupTreeModel{
    let rootLabel:String
    let rootImage:UIImage?
    let children:[groupTreeModel]
    var isCollapsed:Bool
    
    init(label:String,image:UIImage? = nil, children: [groupTreeModel] = [], isCollapsed: Bool = true)
    {
        self.rootLabel = label
        self.rootImage = image
        self.isCollapsed = isCollapsed
        self.children = children
        
    }
    
}