//
//  Agreement.h
//  ios_server
//
//  Created by Liyuan Liu on 11/21/15.
//  Copyright © 2015 Liyuan Liu. All rights reserved.
//

#ifndef Agreement_h
#define Agreement_h

//client send to server
#define login "001"
#define register "002"
#define logout "003"
#define check "004"
#define add "005"

#define online "051"
#define invisible "052"
#define busy "053"


//server send to client
#define errLoginUsernameNotExist "41"
#define errLoginUsernameAlreadyOnline "46"
#define errLoginPassword "47"

#define errRegisterUsernameExist "42"
#define errLogout "43"

#define errAddOffline "44"
#define errAddNotExist "48"

#define userList "105"
#define successLogin "101"
#define successRegister "102"
#define successLogout "103"

struct userList
{
    char username[20];
    char password[50];
    char status[4];
    char *ip;
    
};

#endif /* Agreement_h */











